use thread_manager::hash_key_operations::HashKeyOperations;

pub mod utilities;
pub mod utilities_operations;
pub mod utilities_materials;
pub mod utilities_materials_operations;
pub mod utilities_services;

pub fn get_terminal_size
(
) -> (usize, usize)
{
    let temp = termion::terminal_size().unwrap();
    let remp = (temp.0 as usize, temp.1 as usize);
    remp
}
pub fn tui_utilities_standard_input_key
(    
) -> String
{
    String::from("tui_utilities_standard_input")
}
pub fn tui_utilities_standard_output_key
(    
) -> String
{
    String::from("tui_utilities_standard_output")
}
pub fn tui_utilities_cursor_key
(
) -> String
{
    String::from("tui_utilities_cursor")
}
pub fn tui_utilities_component_list_key
(
) -> String
{
    String::from("tui_utilities_component_list")
}
/// Use casting and an embedded global_list in a owned 
/// type to get access to the materials TROBS in 
/// the manifest. Implement the traits of each TROB on 
/// that sacrificial structure so that you can use the 
/// functionality of the structures in the manifest.
pub fn tui_utilities_manifest
(
    program_name:String,
    components:Option<Vec<std::sync::Arc<crate::utilities::component_save::ComponentSave>>>,
    cursor_position:Option<std::sync::Arc<crate::utilities::position::Position>>
) -> std::sync::Arc<thread_manager::manifest::Manifest>
{
    let temp = 
    thread_manager::manifest::Manifest::new_manifest
    (
        String::from("tui_utilities_manifest"), 
        vec!
        [
            {
                let temp = crate::utilities_materials::standard_input::StandardInput::new_standard_input();
                temp.clone().set_hash_key_name(Some(String::from("tui_utilities_standard_input")));
                temp
            },
            {
                let temp = crate::utilities_materials::standard_output::StandardOutput::new_standard_output();
                temp.clone().set_hash_key_name(Some(String::from("tui_utilities_standard_output")));
                temp
            },
            {
                let temp =
                crate::utilities_materials::cursor::Cursor::new_cursor
                (
                    cursor_position, 
                    None, 
                    None, 
                    None
                );
                temp.clone().set_hash_key_name(Some(String::from("tui_utilities_cursor")));
                temp
            },

            {
                let temp =
                crate::utilities_materials::component_list::ComponentList::new_component_list
                (
                    program_name, 
                    components
                );
                temp.clone().set_hash_key_name(Some(String::from("tui_utilities_component_list")));
                temp
            },
        ],
    );
    temp
}