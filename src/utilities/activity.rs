pub struct Activity
{
    pub(crate) active:std::sync::Mutex<bool>
}
impl Activity
{
    /// Creates an ```Activity``` and defaults to ```false```.
    pub fn new_activity
    (
        value:Option<bool>
    ) -> std::sync::Arc<Self>
    {
        std::sync::Arc::new 
        (
            Activity
            {
                active:
                {
                    if value.is_some()
                    {
                        std::sync::Mutex::new 
                        (
                            value.unwrap()
                        )
                    }
                    else
                    {
                        std::sync::Mutex::new 
                        (
                            false
                        )
                    }
                }
            }
        )
    } 
}