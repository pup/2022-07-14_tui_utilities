pub struct Bounds
{
    pub(crate) horizontal_size:std::sync::Mutex<usize>,
    pub(crate) vertical_size:std::sync::Mutex<usize>,
}
impl Bounds
{
    pub fn new_bounds
    (
        horizontal_size:usize, 
        vertical_size:usize
    ) -> std::sync::Arc<Self>
    {
        std::sync::Arc::new 
        (
            Bounds
            {
                horizontal_size:
                std::sync::Mutex::new
                (
                    horizontal_size
                ),
                vertical_size:
                std::sync::Mutex::new
                (
                    vertical_size
                ),
            }
        )
    }
}