pub struct Component
{
    pub(crate) material_id:Option<std::sync::Arc<thread_manager::material_id::MaterialId>>,
    pub(crate) component_index:std::sync::Mutex<Option<usize>>,
    pub(crate) type_name:std::sync::Mutex<Option<String>>,
    pub(crate) position:Option<std::sync::Arc<crate::utilities::position::Position>>,
    pub(crate) bounds:Option<std::sync::Arc<crate::utilities::bounds::Bounds>>,
    pub(crate) activity:Option<std::sync::Arc<crate::utilities::activity::Activity>>,
}
impl Component
{
    pub fn new_component
    (
        material_id:Option<std::sync::Arc<thread_manager::material_id::MaterialId>>,
        component_index:Option<usize>,
        type_name:Option<String>,
        position:Option<std::sync::Arc<crate::utilities::position::Position>>,
        bounds:Option<std::sync::Arc<crate::utilities::bounds::Bounds>>
    ) -> std::sync::Arc<Self>
    {
        std::sync::Arc::new 
        (
            Component
            { 
                material_id:material_id,
                component_index:
                std::sync::Mutex::new 
                (
                    component_index
                ),
                type_name:
                std::sync::Mutex::new 
                (
                    type_name
                ),
                position:position,
                bounds:bounds,
                activity:None,
            }
        )
    }
}