
#[
    derive
    ( 
        serde::Serialize, 
        serde::Deserialize
    )
]
pub struct 
ComponentSave
{
    pub(crate) menu_material_id:std::sync::Mutex<usize>,
    pub(crate) component_material_id:std::sync::Mutex<usize>,
    pub(crate) component_type_name:std::sync::Mutex<String>,
    pub(crate) data:std::sync::Mutex<Vec<String>>,
}
impl ComponentSave
{
    pub fn new_component_save
    (
        menu_material_id:usize,
        component_material_id:usize,
        component_type_name:String,
        data:Option<Vec<String>>
    ) -> std::sync::Arc<Self>
    {
        std::sync::Arc::new 
        (
            ComponentSave 
            { 
                menu_material_id: 
                std::sync::Mutex::new 
                (
                    menu_material_id
                ),
                component_material_id: 
                std::sync::Mutex::new 
                (
                    component_material_id
                ),
                component_type_name: 
                std::sync::Mutex::new 
                (
                    component_type_name
                ),
                data: 
                {
                    if data.is_none() 
                    { 
                        std::sync::Mutex::new 
                        (
                            Vec::new() 
                        )
                    }
                    else
                    {
                        std::sync::Mutex::new 
                        (
                            data.unwrap()
                        )
                    }
                } 
            }
        )
    }
    pub fn set_component_save
    (
        self:&std::sync::Arc<Self>,
        component_save:&std::sync::Arc<Self>
    )
    {
        *self.menu_material_id.lock().unwrap() = component_save.menu_material_id.lock().unwrap().clone();
        *self.component_material_id.lock().unwrap() = component_save.component_material_id.lock().unwrap().clone();
        *self.component_type_name.lock().unwrap() = component_save.component_type_name.lock().unwrap().clone();
        *self.data.lock().unwrap() = component_save.data.lock().unwrap().clone();
    }
    pub fn set_menu_material_id
    (
        self:&std::sync::Arc<Self>,
        menu_material_id:usize
    )
    {
        *self.menu_material_id.lock().unwrap() = menu_material_id;
    }
    pub fn get_menu_material_id
    (
        self:&std::sync::Arc<Self>
    ) -> usize
    {
        self.menu_material_id.lock().unwrap().clone()
    }
    pub fn set_component_material_id
    (
        self:&std::sync::Arc<Self>,
        component_material_id:usize
    )
    {
        *self.component_material_id.lock().unwrap() = component_material_id;
    }
    pub fn get_component_material_id
    (
        self:&std::sync::Arc<Self>
    ) -> usize
    {
        self.component_material_id.lock().unwrap().clone()
    }
    pub fn set_component_type_name
    (
        self:&std::sync::Arc<Self>,
        component_type_name:String
    )
    {
        *self.component_type_name.lock().unwrap() = component_type_name;
    }
    pub fn get_component_type_name
    (
        self:&std::sync::Arc<Self>
    ) -> String
    {
        self.component_type_name.lock().unwrap().clone()
    }
    pub fn push
    (
        self:&std::sync::Arc<Self>,
        value:String
    )
    {
        self.data.lock().unwrap().push(value);
    }
    pub fn insert
    (
        self:&std::sync::Arc<Self>,
        value:String,
        index:usize
    )
    {
        if index > self.get_data_len()
        {
            self.push(value)
        }
        else 
        {
            self.data.lock().unwrap().insert(index, value)
        }
    }
    pub fn remove
    (
        self:&std::sync::Arc<Self>,
        index:usize
    )
    {
        if index > self.get_data_len() {return}
        else
        {
            self.data.lock().unwrap().remove(index);
        }
    }
    pub fn get_data 
    (
        self:&std::sync::Arc<Self>,
        index:usize,
    ) -> Option<String>
    {
        if index >= self.get_data_len()
        {
            Some
            (
                self.data.lock().unwrap()[index].clone()
            )
        }
        else 
        {
            None
        }
    }
    pub fn get_data_len
    (
        self:&std::sync::Arc<Self>,
    ) -> usize
    {
        self.data.lock().unwrap().len()
    }
}