use crate::utilities_operations::{bounds_operations::BoundsOperations, listing_bounds_operations::ListingBoundsOperations, position_operations::PositionOperations};

pub struct Indices
{
    pub(crate) vertical:std::sync::Mutex<Option<usize>>,
    pub(crate) horizontal:std::sync::Mutex<Option<usize>>,
}
impl Indices
{
    pub fn new_indices
    (
        vertical:Option<usize>,
        horizontal:Option<usize>,
    ) -> std::sync::Arc<Self>
    {
        std::sync::Arc::new 
        (
            Indices
            {
                vertical:
                std::sync::Mutex::new 
                (
                    vertical
                ),
                horizontal:
                std::sync::Mutex::new 
                (
                    horizontal
                ),
            }
        )
    }
    pub fn calculate_new_indices
    (
        owner_bounds:&std::sync::Arc<crate::utilities::bounds::Bounds>,
        owner_position:&std::sync::Arc<crate::utilities::position::Position>,
        owner_listing_bounds:&std::sync::Arc<crate::utilities::listing_bounds::ListingBounds>,
        pointer_position:&std::sync::Arc<crate::utilities::position::Position>
    ) -> Option<std::sync::Arc<Self>>
    {
        if  owner_bounds.clone().pointer_in_bounds(owner_position, pointer_position) && 
            owner_listing_bounds.clone().horizontal_listing_bounds_is_some() && 
            owner_listing_bounds.clone().vertical_listing_bounds_is_some()
        {
            let horizontal = owner_listing_bounds.clone().get_listing_bounds_left().unwrap() + (owner_position.clone().get_x_position().unwrap() + owner_bounds.clone().get_horizontal_bounds_size() - pointer_position.clone().get_x_position().unwrap());
            let vertical = owner_listing_bounds.clone().get_listing_bounds_up().unwrap() + (owner_position.clone().get_y_position().unwrap() + owner_bounds.clone().get_vertical_bounds_size() - pointer_position.clone().get_y_position().unwrap());
            return Some(crate::utilities::indices::Indices::new_indices(Some(vertical), Some(horizontal)));
        }
        else
        {
            None
        }
    }
}