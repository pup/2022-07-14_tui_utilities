The timer structure will compare a give duration to 
a sample of a difference between a marked duration and 
the current elapsed duration. A function within an 
operator or a material can start a timer and check to 
see if that timer is done with the on_done function.