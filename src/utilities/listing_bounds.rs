pub struct ListingBounds
{
    pub(crate) buffer:std::sync::Mutex<Vec<Option<usize>>>,
}
impl ListingBounds
{
    pub fn new_listing_bounds
    (
        left:Option<usize>,
        right:Option<usize>, 
        up:Option<usize>, 
        down:Option<usize>
    ) -> std::sync::Arc<Self>
    {
        std::sync::Arc::new 
        (
            ListingBounds
            {
                buffer:
                std::sync::Mutex::new 
                (
                    vec!
                    [
                            left,
                            right,
                            up,
                            down,
                    ]
                ),
            }
        )
    }
}