use crate::utilities_operations::component_operations::ComponentOperations;

use thread_manager::material_id_operations::MaterialIdOperations;

pub struct Menu
{
    pub(crate) material_id:std::sync::Mutex<Option<std::sync::Arc<thread_manager::material_id::MaterialId>>>,
    pub(crate) position:std::sync::Mutex<Option<std::sync::Arc<crate::utilities::position::Position>>>,
    pub(crate) bounds:std::sync::Mutex<Option<std::sync::Arc<crate::utilities::bounds::Bounds>>>,
    pub(crate) components:std::sync::Mutex<Option<Vec<std::sync::Arc<crate::utilities::component::Component>>>>,
    pub(crate) activity:std::sync::Mutex<Option<std::sync::Arc<crate::utilities::activity::Activity>>>,
    pub(crate) active_component:std::sync::Mutex<Option<usize>>,
}
impl Menu
{
    pub fn new_menu(
        material_id:Option<std::sync::Arc<thread_manager::material_id::MaterialId>>,
        position:Option<std::sync::Arc<crate::utilities::position::Position>>,
        bounds:Option<std::sync::Arc<crate::utilities::bounds::Bounds>>,
        components:Option<Vec<std::sync::Arc<crate::utilities::component::Component>>>,
        activity:Option<std::sync::Arc<crate::utilities::activity::Activity>>,
        active_component:Option<usize>,
    ) -> std::sync::Arc<Self>
    {
        std::sync::Arc::new 
        (
            Menu
            {  
                material_id:
                std::sync::Mutex::new 
                (
                    {
                        if material_id.is_none()
                        {
                            Some
                            (
                                thread_manager::material_id::MaterialId::new_material_id(None, None)
                            )
                        }
                        else 
                        {
                            material_id    
                        }
                    }
                ),
                position:
                std::sync::Mutex::new 
                (
                    position
                ),
                bounds:
                std::sync::Mutex::new 
                (
                    bounds
                ),
                components:
                std::sync::Mutex::new 
                (
                    {
                        if components.is_none()
                        {
                            Some(Vec::new())
                        }
                        else 
                        {
                            for i in 0..components.as_ref().unwrap().len()
                            {
                                components.as_ref().unwrap()[i].clone().set_component_index(Some(i));
                            }
                            components
                        }
                    }
                ),
                activity:
                std::sync::Mutex::new 
                (
                    activity
                ),
                active_component:
                std::sync::Mutex::new 
                (
                    active_component
                ),
            }
        )
    }
    pub fn menu_metadata
    (
        self:&std::sync::Arc<Self>,
    ) -> std::sync::Arc<Self>
    {
        std::sync::Arc::new 
        (
            Menu
            { 
                material_id:
                std::sync::Mutex::new 
                (
                    Some
                    (
                        {
                            let temp = thread_manager::material_id::MaterialId::new_material_id(None, None);
                            temp.clone().set_material_id(self.clone().material_id.lock().unwrap().as_ref().unwrap().clone());
                            temp
                        }
                    )
                ),
                position:
                std::sync::Mutex::new 
                (
                    None
                ),
                bounds:
                std::sync::Mutex::new 
                (
                    None
                ),
                components:
                std::sync::Mutex::new
                (
                    None
                ),
                activity:
                std::sync::Mutex::new 
                (
                    None
                ),
                active_component:
                std::sync::Mutex::new
                (
                    None
                )
            }
        )
    }
}