pub struct Position
{
    pub(crate) x:std::sync::Mutex<Option<usize>>,
    pub(crate) y:std::sync::Mutex<Option<usize>>,
}
impl Position
{
    pub fn new_position
    (
        x:Option<usize>, 
        y:Option<usize>
    ) -> std::sync::Arc<Self>
    {
        std::sync::Arc::new 
        (
            Position
            {
                x:
                std::sync::Mutex::new
                (
                    x
                ),
                y:
                std::sync::Mutex::new
                (
                    y
                ),
            }
        )
    }
}