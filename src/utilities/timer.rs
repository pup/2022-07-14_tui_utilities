use crate::utilities_operations::timer_operations::TimerOperations;

pub struct Timer
{
    pub(crate) system_time:std::sync::Mutex<Option<std::time::SystemTime>>,
    pub(crate) duration:std::sync::Mutex<Option<std::time::Duration>>,
    pub(crate) triggered:std::sync::Mutex<bool>,
    pub(crate) done:std::sync::Mutex<bool>,
}
impl Timer
{
    pub fn new_timer
    (
        duration:Option<std::time::Duration>
    ) -> std::sync::Arc<Self>
    {
        std::sync::Arc::new 
        (
            Timer
            {
                system_time:
                std::sync::Mutex::new 
                (
                    None
                ),
                duration:
                std::sync::Mutex::new 
                (
                    duration
                ),
                triggered:
                std::sync::Mutex::new 
                (
                    false
                ),
                done:
                std::sync::Mutex::new 
                (
                    false
                ),
            }
        )
    }
    pub(crate) fn set_triggered
    (
        self:&std::sync::Arc<Self>, 
        value:bool
    )
    {
        *self.triggered.lock().unwrap() = value;
    }
    pub(crate) fn get_triggered
    (
        self:&std::sync::Arc<Self>,
    ) -> bool
    {
        self.triggered.lock().unwrap().clone()
    }
    pub(crate) fn get_system_time_elapsed
    (
        self:&std::sync::Arc<Self>,
    ) -> Option<std::time::Duration>
    {
        if self.system_time_is_some()
        {
            let temp = std::time::SystemTime::now();
            Some(temp.duration_since(self.system_time.lock().unwrap().unwrap()).unwrap())
        }
        else
        {
            None
        }
    }
    pub(crate) fn system_time_is_some
    (
        self:&std::sync::Arc<Self>,
    ) -> bool
    {
        self.system_time.lock().unwrap().is_some()
    }
    pub(crate) fn system_time_equals_duration
    (
        self:&std::sync::Arc<Self>,
    ) -> bool
    {
        let elapsed = self.get_system_time_elapsed();
        let duration = self.clone().get_timer_duration();
        if elapsed.is_some() && duration.is_some()
        {
            elapsed >= duration
        }
        else
        {
            false
        }
    }
    pub(crate) fn get_done
    (
        self:&std::sync::Arc<Self>,
    ) -> bool
    {
        self.done.lock().unwrap().clone()
    }
    pub(crate) fn set_done
    (
        self:&std::sync::Arc<Self>, 
        value:bool
    )
    {
        *self.done.lock().unwrap() = value;
    }
}