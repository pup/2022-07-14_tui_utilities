use crate::{utilities_operations::menu_save_operations::MenuSaveOperations, utilities_materials_operations::component_list_operations::ComponentListOperations};

#[
    derive
    (
        serde::Serialize,
        serde::Deserialize,
    )
]
pub struct ComponentList
{
    pub(crate) material:std::sync::Arc<thread_manager::materials::Materials>,
    pub(crate) program_name:std::sync::Mutex<String>,
    pub(crate) components:std::sync::Mutex<Vec<std::sync::Arc<crate::utilities::component_save::ComponentSave>>>,
}
impl ComponentList
{
    pub fn new_component_list
    (
        program_name:String,
        components:Option<Vec<std::sync::Arc<crate::utilities::component_save::ComponentSave>>>,
    ) -> std::sync::Arc<Self>
    {
        std::sync::Arc::new 
        (
            ComponentList 
            {
                material:
                thread_manager::materials::Materials::new_material(),
                program_name:
                std::sync::Mutex::new 
                (
                    program_name
                ),
                components:
                {
                    if components.is_none()
                    {
                        std::sync::Mutex::new 
                        (
                            Vec::new()
                        )
                    }
                    else
                    {
                        std::sync::Mutex::new
                        (
                            components.unwrap()
                        )
                    }
                }
            }
        )
    }
    pub(crate) fn initialize_configuration_folder
    (
        self:&std::sync::Arc<Self>,
    ) -> std::path::PathBuf
    {
        let mut config_directory = dirs::config_dir().unwrap();
        config_directory.push(format!("/{}", self.clone().get_program_name()));
        let mut components_file = config_directory.clone();
        components_file.push("/components.json");
        if  config_directory.try_exists().is_ok() &&
            components_file.try_exists().is_ok() {return components_file;}
        std::fs::create_dir(config_directory.clone()).unwrap();
        std::fs::File::create(components_file.clone()).unwrap(); 
        components_file
    }
}
pub fn load
(   
    materials:&Vec<std::sync::Arc<dyn thread_manager::materials_operations::MaterialsOperations>>
)
{
    let global_list = materials[0].clone().any_materials().downcast::<thread_manager::global_list::GlobalList>().unwrap();
    let component_list = materials[1].clone().any_materials().downcast::<crate::utilities_materials::component_list::ComponentList>().unwrap();
    let cursor = materials[2].clone().any_materials().downcast::<crate::utilities_materials::cursor::Cursor>().unwrap();
    for i in 2..materials.len()
    {
        materials[i].clone().any_materials().clone().downcast::<crate::utilities::menu::Menu>().unwrap().update(&component_list.clone().get_components(), &cursor.clone(), &global_list.clone());
    }
}