use crate::utilities_materials_operations::cursor_operations::CursorOperations;

pub struct Cursor
{
    pub(crate) material:std::sync::Arc<thread_manager::materials::Materials>,
    pub(crate) position:std::sync::Arc<crate::utilities::position::Position>,
    pub(crate) terminal_size_x:std::sync::Mutex<usize>,
    pub(crate) terminal_size_y:std::sync::Mutex<usize>,
    pub(crate) cursor_position_x:std::sync::Mutex<usize>,
    pub(crate) cursor_position_y:std::sync::Mutex<usize>,
    pub(crate) menu:std::sync::Mutex<Option<std::sync::Arc<crate::utilities::menu::Menu>>>,
    pub(crate) component:std::sync::Mutex<Option<std::sync::Arc<crate::utilities::component::Component>>>,
    pub(crate) _cursor_hidden:std::sync::Mutex<Option<bool>>,
}
impl Cursor
{
    pub fn new_cursor
    (
        position:Option<std::sync::Arc<crate::utilities::position::Position>>,
        menu:Option<std::sync::Arc<crate::utilities::menu::Menu>>,
        component:Option<std::sync::Arc<crate::utilities::component::Component>>,
        _cursor_hidden:Option<bool>
    ) -> std::sync::Arc<Self>
    {
        let temp = std::sync::Arc::new
        (
            Cursor
            {
                material:
                thread_manager::materials::Materials::new_material(),
                position:
                {
                    if position.is_some()
                    {
                        position.unwrap()
                    }
                    else
                    {
                        crate::utilities::position::Position::new_position(None, None)
                    }
                },
                terminal_size_x:
                std::sync::Mutex::new 
                (
                    0
                ),
                terminal_size_y:
                std::sync::Mutex::new 
                (
                    0
                ),
                cursor_position_x:
                std::sync::Mutex::new 
                (
                    1
                ),
                cursor_position_y:
                std::sync::Mutex::new 
                (
                    1
                ),
                menu:
                std::sync::Mutex::new 
                (
                    menu
                ),
                component:
                std::sync::Mutex::new 
                (
                    component
                ),
                _cursor_hidden:
                std::sync::Mutex::new 
                (
                    _cursor_hidden
                )
            }
        );
        temp.clone().configure_cursor_position();
        return temp;
    }
}