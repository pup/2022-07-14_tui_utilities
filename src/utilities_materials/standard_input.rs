pub struct StandardInput
{
    pub(crate) materials:std::sync::Arc<thread_manager::materials::Materials>,
    pub(crate) input_stream:std::sync::RwLock<Vec<termion::event::Event>>,
    pub(crate) standard_input:std::io::Stdin,
}
impl StandardInput
{
    pub fn new_standard_input
    (
    ) -> std::sync::Arc<Self>
    {
        std::sync::Arc::new 
        (
            StandardInput
            {
                materials:
                thread_manager::materials::Materials::new_material(),
                input_stream:
                std::sync::RwLock::new 
                (
                    Vec::new()
                ),
                standard_input:
                std::io::stdin()
            }
        )
    }

}