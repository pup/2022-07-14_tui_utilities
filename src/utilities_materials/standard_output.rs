pub struct StandardOutput
{
    pub(crate) materials:std::sync::Arc<thread_manager::materials::Materials>,
    pub(crate) standard_out:std::io::Stdout,
}
impl StandardOutput
{
    pub fn new_standard_output
    (
    ) -> std::sync::Arc<Self>
    {
        std::sync::Arc::new
        (
            StandardOutput
            {
                materials:
                thread_manager::materials::Materials::new_material(),
                standard_out:
                std::io::stdout()
            }
        )
    }
}