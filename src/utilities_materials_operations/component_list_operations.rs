use thread_manager::materials_operations::MaterialsOperations;

pub trait ComponentListOperations:Send + Sync + 'static
{
    fn get_component_list
    (
        self:std::sync::Arc<Self>
    ) -> std::sync::Arc<crate::utilities_materials::component_list::ComponentList>;
    fn any_component_list
    (
        self:std::sync::Arc<Self>
    ) -> std::sync::Arc<dyn std::any::Any + Send + Sync + 'static>;
    fn clear_component_list
    (
        self:std::sync::Arc<Self>
    )
    {

    }
    fn component_list_is_some
    (
        self:std::sync::Arc<Self>
    ) -> bool
    {
        true
    }
    fn set_component_list
    (
        self:std::sync::Arc<Self>,
        component_list:&std::sync::Arc<crate::utilities_materials::component_list::ComponentList>
    )
    {
        self.clone().get_component_list().set_materials(&component_list.clone().get_materials());
        *self.clone().get_component_list().program_name.lock().unwrap() = component_list.program_name.lock().unwrap().clone();
        *self.clone().get_component_list().components.lock().unwrap() = component_list.components.lock().unwrap().clone();
    }
    fn set_program_name
    (
        self:std::sync::Arc<Self>,
        program_name:String
    )
    {
        *self.clone().get_component_list().program_name.lock().unwrap() = program_name;
    }
    fn get_program_name
    (
        self:std::sync::Arc<Self>,
    ) -> String
    {
        self.clone().get_component_list().program_name.lock().unwrap().clone()
    }
    fn set_components
    (
        self:std::sync::Arc<Self>,
        components:Vec<std::sync::Arc<crate::utilities::component_save::ComponentSave>>
    )
    {
        *self.clone().get_component_list().components.lock().unwrap() = components;
    }
    fn get_components
    (
        self:std::sync::Arc<Self>,
    ) -> Vec<std::sync::Arc<crate::utilities::component_save::ComponentSave>>
    {
        self.clone().get_component_list().components.lock().unwrap().clone()
    }
    fn push
    (
        self:std::sync::Arc<Self>,
        value:std::sync::Arc<crate::utilities::component_save::ComponentSave>,
    )
    {
        self.clone().get_component_list().components.lock().unwrap().push(value);
    }
    fn insert
    (
        self:std::sync::Arc<Self>,
        value:std::sync::Arc<crate::utilities::component_save::ComponentSave>,
        index:usize
    )
    {
        if index > self.clone().get_component_list().components.lock().unwrap().len()
        {
            self.push(value)
        }
        else 
        {
            self.clone().get_component_list().components.lock().unwrap().insert(index, value)
        }
    }
    fn remove
    (
        self:std::sync::Arc<Self>,
        index:usize
    )
    {
        if index > self.clone().get_component_list().components.lock().unwrap().len() {return}
        else
        {
            self.clone().get_component_list().components.lock().unwrap().remove(index);
        }
    }
    fn save
    (
        self:std::sync::Arc<Self>,
        value:std::sync::Arc<crate::utilities::component_save::ComponentSave>,
    )
    {
        self.clone().get_component_list().components.lock().unwrap().push(value)
    }
    fn save_all
    (
        self:std::sync::Arc<Self>,
    ) -> Option<String>
    {
        let opened_components_file = std::fs::File::options().read(true).write(true).truncate(true).open(self.clone().get_component_list().initialize_configuration_folder());
        if opened_components_file.is_err() {return Some(opened_components_file.err().unwrap().to_string());}
        let json_string:String = serde_json::to_string_pretty(&self.clone().get_component_list()).unwrap();
        let json_sting_write = serde_json::to_writer_pretty(opened_components_file.unwrap(), &json_string);
        if json_sting_write.is_err() {return Some(json_sting_write.err().unwrap().to_string());}
        None
    }
}
impl crate::utilities_materials_operations::component_list_operations::ComponentListOperations for crate::utilities_materials::component_list::ComponentList
{
    fn get_component_list
    (
        self:std::sync::Arc<Self>
    ) -> std::sync::Arc<crate::utilities_materials::component_list::ComponentList> 
    {
        self.clone()    
    }
    fn any_component_list
    (
        self:std::sync::Arc<Self>
    ) -> std::sync::Arc<dyn std::any::Any + Send + Sync + 'static> 
    {
        self.clone()    
    }
}
impl thread_manager::materials_operations::MaterialsOperations for crate::utilities_materials::component_list::ComponentList
{
    fn get_materials
    (
        self:std::sync::Arc<Self>
    ) -> std::sync::Arc<thread_manager::materials::Materials> 
    {
        self.clone().material.clone()
    }
    fn any_materials
    (
        self:std::sync::Arc<Self>
    ) -> std::sync::Arc<dyn std::any::Any + Send + Sync + 'static> 
    {
        self.clone()    
    }
}
impl thread_manager::hash_key_operations::HashKeyOperations for crate::utilities_materials::component_list::ComponentList
{
    fn get_hash_key
    (
        self:std::sync::Arc<Self>,
    ) -> std::sync::Arc<thread_manager::hash_key::HashKey> 
    {
        self.clone().get_materials().get_hash_key() 
    }
    fn any_hash_key
    (
        self:std::sync::Arc<Self>,
    ) -> std::sync::Arc<dyn std::any::Any + Send + Sync + 'static> 
    {
        self.clone()    
    }
}
impl Clone for crate::utilities_materials::component_list::ComponentList
{
    fn clone
    (
        &self
    ) -> crate::utilities_materials::component_list::ComponentList 
    {
        crate::utilities_materials::component_list::ComponentList 
        {
            material:
            thread_manager::materials::Materials::new_material(),
            program_name:
            std::sync::Mutex::new 
            (
                self.program_name.lock().unwrap().clone()
            ),
            components:
            {

                std::sync::Mutex::new
                (
                    self.components.lock().unwrap().iter().map
                    (
                        |component_save|
                        {
                            std::sync::Arc::new((**component_save).clone())
                        }
                    ).collect()
                )
            }
        }    
    }
}