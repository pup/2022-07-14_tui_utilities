use thread_manager::materials_operations::MaterialsOperations;

use crate::utilities_operations::{menu_operations::MenuOperations, bounds_operations::BoundsOperations, position_operations::PositionOperations, component_operations::ComponentOperations};

use super::standard_output_operations::StandardOutputOperations;

pub trait CursorOperations
{
    fn get_cursor
    (
        self:std::sync::Arc<Self>,
    ) -> std::sync::Arc<crate::utilities_materials::cursor::Cursor>;
    fn any_cursor
    (
        self:std::sync::Arc<Self>,
    ) -> std::sync::Arc<dyn std::any::Any + Send + Sync + 'static>;
    fn clear_cursor
    (
        self:std::sync::Arc<Self>
    )
    {

    }
    fn cursor_is_some
    (
        self:std::sync::Arc<Self>
    ) -> bool
    {
        true
    }
    fn configure_cursor_position
    (
        self:std::sync::Arc<Self>
    )
    {
        if  self.clone().get_cursor().get_menu().horizontal_bounds_size_not_equal_zero() && 
            self.clone().get_cursor().get_menu().vertical_bounds_size_not_equal_zero() && 
            self.clone().get_cursor().has_x_position_and_y_position()
        {
            self.clone().move_cursor(&self.clone().get_cursor().get_position(), None);
        }
        else
        {
            self.clone().get_cursor().set_position(&crate::utilities::position::Position::new_position(Some(1), Some(1)));
            self.clone().move_cursor(&self.clone().get_cursor().get_position(), None);
        }
    }
    fn update_cursor_index
    (
        self:std::sync::Arc<Self>, 
        owner:std::sync::Arc<impl crate::utilities_operations::bounds_operations::BoundsOperations +
        crate::utilities_operations::position_operations::PositionOperations +
        crate::utilities_operations::listing_bounds_operations::ListingBoundsOperations +
        crate::utilities_operations::indices_operations::IndicesOperations +
        Send +
        Sync +
        'static>
    )
    {
        owner.clone().update_indices
        (
            &owner.clone().get_bounds(), 
            &owner.clone().get_position(),
            &owner.clone().get_listing_bounds(),
            &self.clone().get_cursor().get_position()
        )
    }
    fn move_check_menu
    (
        self:std::sync::Arc<Self>,
        new_position:&std::sync::Arc<crate::utilities::position::Position>,
    ) -> bool
    {
         self.clone().get_cursor().menu_is_some() &&   
         self.clone().get_cursor().get_menu().pointer_in_bounds(&self.clone().get_cursor().get_menu().get_position(),new_position) && 
        !self.clone().get_cursor().get_menu().pointer_on_bounds_left(&self.clone().get_cursor().get_menu().get_position(), new_position) &&
        !self.clone().get_cursor().get_menu().pointer_on_bounds_right(&self.clone().get_cursor().get_menu().get_position(), new_position) &&
        !self.clone().get_cursor().get_menu().pointer_on_bounds_top(&self.clone().get_cursor().get_menu().get_position(), new_position) &&
        !self.clone().get_cursor().get_menu().pointer_on_bounds_bottom(&self.clone().get_cursor().get_menu().get_position(), new_position)
    }
    fn move_cursor
    (
        self:std::sync::Arc<Self>, 
        new_position:&std::sync::Arc<crate::utilities::position::Position>,
        length:Option<usize>
    )
    {
        if   self.clone().get_cursor().menu_is_some() &&   
             self.clone().get_cursor().get_menu().pointer_in_bounds(&self.clone().get_cursor().get_menu().get_position(),new_position) && 
            !self.clone().get_cursor().get_menu().pointer_on_bounds_left(&self.clone().get_cursor().get_menu().get_position(), new_position) &&
            !self.clone().get_cursor().get_menu().pointer_on_bounds_right(&self.clone().get_cursor().get_menu().get_position(), new_position) &&
            !self.clone().get_cursor().get_menu().pointer_on_bounds_top(&self.clone().get_cursor().get_menu().get_position(), new_position) &&
            !self.clone().get_cursor().get_menu().pointer_on_bounds_bottom(&self.clone().get_cursor().get_menu().get_position(), new_position)
        {
            self.clone().get_cursor().set_position(new_position);
            print!("{}", termion::cursor::Goto(self.clone().get_cursor().get_x_position().unwrap() as u16, self.clone().get_cursor().get_y_position().unwrap() as u16));
        }
        else if  self.clone().get_cursor().component_is_some()&&
                 self.clone().get_cursor().get_component().pointer_in_bounds(&self.clone().get_cursor().get_component().get_position(), new_position) &&
                !self.clone().get_cursor().get_component().pointer_on_bounds_left(&self.clone().get_cursor().get_component().get_position(), new_position) &&
                !self.clone().get_cursor().get_component().pointer_on_bounds_right(&self.clone().get_cursor().get_component().get_position(), new_position) &&
                !self.clone().get_cursor().get_component().pointer_on_bounds_top(&self.clone().get_cursor().get_component().get_position(), new_position) &&
                !self.clone().get_cursor().get_component().pointer_on_bounds_bottom(&self.clone().get_cursor().get_component().get_position(), new_position)
        {
            if  length.is_some() && 
                self.clone().get_cursor().get_component().horizontal_bounds_greater_than_length(length.unwrap()) &&
                self.clone().get_cursor().get_component().calculate_pointer_bounds_distance(&self.clone().get_cursor().get_component().get_position(), new_position).unwrap().0.unwrap() < length.unwrap() + 1
            {
                self.clone().get_cursor().get_component().set_position(new_position);
                print!("{}", termion::cursor::Goto(self.clone().get_cursor().get_component().get_x_position().unwrap() as u16, self.clone().get_cursor().get_component().get_y_position().unwrap() as u16));
            }
            else
            {
                self.clone().get_cursor().get_component().set_position(new_position);
                print!("{}", termion::cursor::Goto(self.clone().get_cursor().get_component().get_x_position().unwrap() as u16, self.clone().get_cursor().get_component().get_y_position().unwrap() as u16));
            }
        }
    }
    fn cursor_set_terminal_size
    (
        self:std::sync::Arc<Self>, 
        size:(usize, usize)
    )
    {
        *self.clone().get_cursor().terminal_size_x.lock().unwrap() = size.0;
        *self.clone().get_cursor().terminal_size_y.lock().unwrap() = size.1;
    }
    fn cursor_update_terminal_size
    (
        self:std::sync::Arc<Self>
    )
    {
        self.cursor_set_terminal_size(crate::get_terminal_size())
    }
    fn get_cursor_position
    (
        self:std::sync::Arc<Self>,
        standard_output:&std::sync::Arc<crate::utilities_materials::standard_output::StandardOutput>
    )
    {
        let (x, y) = termion::cursor::DetectCursorPos::cursor_pos(&mut standard_output.clone().get_output_stream()).unwrap();
        self.clone().get_cursor().set_position(&crate::utilities::position::Position::new_position(Some(x as usize), Some(y as usize)));
    }
    fn get_cursor_x
    (
        self:std::sync::Arc<Self>,
    ) -> usize
    {
        self.clone().get_cursor().cursor_position_x.lock().unwrap().clone()
    }
    fn get_cursor_y
    (
        self:std::sync::Arc<Self>
    ) -> usize
    {
        self.clone().get_cursor().cursor_position_y.lock().unwrap().clone()
    }
}
impl CursorOperations for crate::utilities_materials::cursor::Cursor
{
    fn get_cursor
    (
        self:std::sync::Arc<Self>,
    ) -> std::sync::Arc<crate::utilities_materials::cursor::Cursor> 
    {
        self.clone()   
    }
    fn any_cursor 
    (
        self:std::sync::Arc<Self>,
    ) -> std::sync::Arc<dyn std::any::Any + Send + Sync + 'static> 
    {
        self.clone()    
    }
}
impl crate::utilities_operations::position_operations::PositionOperations for crate::utilities_materials::cursor::Cursor
{
    fn get_position
    (
        self:std::sync::Arc<Self>
    ) -> std::sync::Arc<crate::utilities::position::Position> 
    {
        self.position.clone()
    }
    fn any_position
    (
        self:std::sync::Arc<Self>
    ) -> std::sync::Arc<dyn std::any::Any + Send + Sync + 'static> 
    {
        self.clone()
    }
}
impl crate::utilities_operations::component_operations::ComponentOperations for crate::utilities_materials::cursor::Cursor
{
    fn get_component
    (
        self:std::sync::Arc<Self>
    ) -> std::sync::Arc<crate::utilities::component::Component> 
    {
        self.component.lock().unwrap().as_ref().unwrap().clone()
    }
    fn any_component
    (
        self:std::sync::Arc<Self>
    ) -> std::sync::Arc<dyn std::any::Any + Send + Sync + 'static> 
    {
        self.clone()
    }
    fn set_component
    (
        self:std::sync::Arc<Self>,
        component:&std::sync::Arc<crate::utilities::component::Component>
    )
    {
        if self.component.lock().unwrap().is_none()
        {
            *self.component.lock().unwrap() = 
            Some
            (
                component.clone()
            )
        }
        else
        {
            self.get_component().set_component(component)
        }
    }
    fn clear_component
    (
        self:std::sync::Arc<Self>
    ) 
    {
       *self.component.lock().unwrap() = None;
    }
    fn component_is_some
    (
        self:std::sync::Arc<Self>
    ) -> bool 
    {
        self.component.lock().unwrap().is_some()
    }
}
impl crate::utilities_operations::menu_operations::MenuOperations for crate::utilities_materials::cursor::Cursor
{
    fn get_menu
    (
        self:std::sync::Arc<Self>
    ) -> std::sync::Arc<crate::utilities::menu::Menu> 
    {
        self.menu.lock().unwrap().as_ref().unwrap().clone() 
    }
    fn any_menu
    (
        self:std::sync::Arc<Self>
    ) -> std::sync::Arc<dyn std::any::Any + Send + Sync + 'static> 
    {
        self.clone()    
    }
    fn set_menu
    (
        self:std::sync::Arc<Self>,
        menu:&std::sync::Arc<crate::utilities::menu::Menu>
    )
    {
        if self.clone().get_cursor().menu_is_some()
        {
            *self.menu.lock().unwrap() = 
            Some
            (
                menu.clone()
            )
        }
        else
        {
            self.get_menu().set_menu(menu)
        }
    }
    fn clear_menu
    (
        self:std::sync::Arc<Self>
    ) 
    {
        *self.menu.lock().unwrap() = None
    }
    fn menu_is_some
    (
        self:std::sync::Arc<Self>
    ) -> bool
    {
        self.menu.lock().unwrap().is_some()
    }
}
impl thread_manager::materials_operations::MaterialsOperations for crate::utilities_materials::cursor::Cursor
{
    fn get_materials
    (
        self:std::sync::Arc<Self>
    ) -> std::sync::Arc<thread_manager::materials::Materials> 
    {
        self.material.clone()  
    }
    fn any_materials
    (
        self:std::sync::Arc<Self>
    ) -> std::sync::Arc<dyn std::any::Any + Send + Sync + 'static> 
    {
        self.clone()    
    }
}
impl thread_manager::hash_key_operations::HashKeyOperations for crate::utilities_materials::cursor::Cursor
{
    fn get_hash_key
    (
        self:std::sync::Arc<Self>,
    ) -> std::sync::Arc<thread_manager::hash_key::HashKey> 
    {
        self.clone().get_materials().get_hash_key() 
    }
    fn any_hash_key
    (
        self:std::sync::Arc<Self>,
    ) -> std::sync::Arc<dyn std::any::Any + Send + Sync + 'static> 
    {
        self.clone()    
    }
}
impl Clone for crate::utilities_materials::cursor::Cursor
{
    fn clone
    (
        &self
    ) -> crate::utilities_materials::cursor::Cursor 
    {
        crate::utilities_materials::cursor::Cursor 
        {
            material:
            thread_manager::materials::Materials::new_material(),
            position:
            {
                std::sync::Arc::new
                (
                    (*self.position).clone()
                )
            },
            terminal_size_x:
            std::sync::Mutex::new 
            (
                self.terminal_size_x.lock().unwrap().clone()
            ),
            terminal_size_y:
            std::sync::Mutex::new 
            (
                self.terminal_size_y.lock().unwrap().clone()
            ),
            cursor_position_x:
            std::sync::Mutex::new 
            (
                self.cursor_position_x.lock().unwrap().clone()
            ),
            cursor_position_y:
            std::sync::Mutex::new 
            (
                self.cursor_position_y.lock().unwrap().clone()
            ),
            menu:
            std::sync::Mutex::new 
            (
                if self.menu.lock().unwrap().is_some()
                {
                    
                    Some
                    (
                        std::sync::Arc::new 
                        (
                            (**self.menu.lock().unwrap().as_ref().unwrap()).clone()
                        )
                    )
                }
                else
                {
                    None
                }
            ),
            component:
            std::sync::Mutex::new 
            (
                {
                    if self.component.lock().unwrap().is_some()
                    {
                        Some
                        (
                            std::sync::Arc::new 
                            (
                                (**self.component.lock().unwrap().as_ref().unwrap()).clone()
                            )
                        )
                    }
                    else 
                    {
                        None
                    }
                }
            ),
            _cursor_hidden:
            std::sync::Mutex::new 
            (
                self._cursor_hidden.lock().unwrap().clone()
            )
        }
    }
}