use termion::input::TermRead;
use thread_manager::materials_operations::MaterialsOperations;

pub trait StandardInputOperations:Send + Sync + 'static
{
    fn get_standard_input
    (
        self:std::sync::Arc<Self>,
    ) -> std::sync::Arc<crate::utilities_materials::standard_input::StandardInput>;
    fn any_standard_input
    (
        self:std::sync::Arc<Self>,
    ) -> std::sync::Arc<dyn std::any::Any + Send + Sync + 'static>;
    fn clear_standard_input
    (
        self:std::sync::Arc<Self>
    )
    {

    }
    fn standard_input_is_some
    (
        self:std::sync::Arc<Self>
    ) -> bool
    {
        true
    }
    fn get_standard_input_lock<'a>
    (
        self:std::sync::Arc<Self>
    ) -> std::io::StdinLock<'a>
    {
        self.clone().get_standard_input().standard_input.lock()
    }
    fn update_stream
    (
        self:std::sync::Arc<Self>,
    )
    {
        let standard_input = self.clone().get_standard_input_lock();
        let mut temp:Vec<termion::event::Event> = Vec::new();
        for event in standard_input.events()
        {
            if event.is_ok()
            {
                temp.push(event.unwrap());
            }
        }
        *self.clone().get_standard_input().input_stream.write().unwrap() = temp;
    }
    fn get_input_stream
    (
        self:std::sync::Arc<Self>,
    ) -> Vec<termion::event::Event>
    {
        self.clone().get_standard_input().input_stream.read().unwrap().clone()
    }
}
impl StandardInputOperations for crate::utilities_materials::standard_input::StandardInput
{
    fn get_standard_input
    (
        self:std::sync::Arc<Self>,
    ) -> std::sync::Arc<crate::utilities_materials::standard_input::StandardInput> 
    {
        self.clone()    
    }
    fn any_standard_input
    (
        self:std::sync::Arc<Self>,
    ) -> std::sync::Arc<dyn std::any::Any + Send + Sync + 'static> 
    {
        self.clone()    
    }
}
impl thread_manager::materials_operations::MaterialsOperations for crate::utilities_materials::standard_input::StandardInput
{
    fn get_materials
    (
        self:std::sync::Arc<Self>
    ) -> std::sync::Arc<thread_manager::materials::Materials> 
    {
        self.clone().materials.clone()    
    }
    fn any_materials
    (
        self:std::sync::Arc<Self>
    ) -> std::sync::Arc<dyn std::any::Any + Send + Sync + 'static> 
    {
        self.clone()    
    }
}
impl thread_manager::hash_key_operations::HashKeyOperations for crate::utilities_materials::standard_input::StandardInput
{
    fn get_hash_key
    (
        self:std::sync::Arc<Self>,
    ) -> std::sync::Arc<thread_manager::hash_key::HashKey> 
    {
        self.clone().get_materials().get_hash_key() 
    }
    fn any_hash_key
    (
        self:std::sync::Arc<Self>,
    ) -> std::sync::Arc<dyn std::any::Any + Send + Sync + 'static> 
    {
        self.clone()    
    }
}
impl Clone for crate::utilities_materials::standard_input::StandardInput
{
    fn clone
    (
        &self
    ) -> crate::utilities_materials::standard_input::StandardInput 
    {
        crate::utilities_materials::standard_input::StandardInput
        {
            materials:
            thread_manager::materials::Materials::new_material(),
            input_stream:
            std::sync::RwLock::new 
            (
                self.input_stream.read().unwrap().clone()
            ),
            standard_input:
            std::io::stdin()
        }    
    }
}