use std::io::{Write, StdoutLock};

use termion::raw::{IntoRawMode, RawTerminal};
use thread_manager::materials_operations::MaterialsOperations;

use crate::utilities_operations::position_operations::PositionOperations;

pub trait StandardOutputOperations:Send + Sync + 'static
{
    fn get_standard_output
    (
        self:std::sync::Arc<Self>
    ) -> std::sync::Arc<crate::utilities_materials::standard_output::StandardOutput>;
    fn any_standard_output
    (
        self:std::sync::Arc<Self>
    ) -> std::sync::Arc<dyn std::any::Any + Send + Sync + 'static>;
    fn clear_standard_output
    (
        self:std::sync::Arc<Self>
    )
    {

    }
    fn standard_output_is_some
    (
        self:std::sync::Arc<Self>
    ) -> bool
    {
        true
    }
    fn get_output_stream<'a>
    (
        self:std::sync::Arc<Self>,
    ) -> RawTerminal<StdoutLock<'a>>
    {
        self.clone().get_standard_output().standard_out.lock().into_raw_mode().unwrap()
    }
    fn print_words
    (
        self:std::sync::Arc<Self>,
        value:String, 
        position:std::sync::Arc<crate::utilities::position::Position>, 
    )
    {
        if position.clone().x_position_is_some() && position.clone().y_position_is_some()
        {
            let mut output_stream = self.get_output_stream();
            write!(output_stream, "{}{}", termion::cursor::Goto(position.clone().get_x_position().unwrap() as u16, position.clone().get_y_position().unwrap() as u16), value).unwrap();
            output_stream.flush().unwrap();
        }
    }
    fn print_words_highlighted
    (
        self:std::sync::Arc<Self>,
        value:String, 
        position:std::sync::Arc<crate::utilities::position::Position>, 
    )
    {
        if position.clone().x_position_is_some() && position.clone().y_position_is_some()
        {
            let mut output_stream = self.get_output_stream();
            write!(output_stream, "{}{}{}{}{}{}", termion::cursor::Goto(position.clone().get_x_position().unwrap() as u16, position.clone().get_y_position().unwrap() as u16), termion::color::Fg(termion::color::Rgb(0, 0, 0)), termion::color::Bg(termion::color::Rgb(255, 255, 255)), value, termion::color::Fg(termion::color::Reset), termion::color::Bg(termion::color::Reset)).unwrap();
            output_stream.flush().unwrap();
        }
    }
}
impl crate::utilities_materials_operations::standard_output_operations::StandardOutputOperations for crate::utilities_materials::standard_output::StandardOutput
{
    fn get_standard_output
    (
        self:std::sync::Arc<Self>
    ) -> std::sync::Arc<crate::utilities_materials::standard_output::StandardOutput> 
    {
        self.clone()    
    }
    fn any_standard_output
    (
        self:std::sync::Arc<Self>
    ) -> std::sync::Arc<dyn std::any::Any + Send + Sync + 'static> 
    {
        self.clone()    
    }
}
impl thread_manager::materials_operations::MaterialsOperations for crate::utilities_materials::standard_output::StandardOutput
{
    fn get_materials
    (
        self:std::sync::Arc<Self>
    ) -> std::sync::Arc<thread_manager::materials::Materials> 
    {
        self.clone().materials.clone()    
    }
    fn any_materials
    (
        self:std::sync::Arc<Self>
    ) -> std::sync::Arc<dyn std::any::Any + Send + Sync + 'static> 
    {
        self.clone()    
    }
}
impl thread_manager::hash_key_operations::HashKeyOperations for crate::utilities_materials::standard_output::StandardOutput
{
    fn get_hash_key
    (
        self:std::sync::Arc<Self>,
    ) -> std::sync::Arc<thread_manager::hash_key::HashKey> 
    {
        self.clone().get_materials().get_hash_key() 
    }
    fn any_hash_key
    (
        self:std::sync::Arc<Self>,
    ) -> std::sync::Arc<dyn std::any::Any + Send + Sync + 'static> 
    {
        self.clone()    
    }
}
impl Clone for crate::utilities_materials::standard_output::StandardOutput
{
    fn clone
    (
        &self
    ) -> crate::utilities_materials::standard_output::StandardOutput 
    {
        crate::utilities_materials::standard_output::StandardOutput
        {
            materials:
            thread_manager::materials::Materials::new_material(),
            standard_out:
            std::io::stdout()
        }    
    }
}