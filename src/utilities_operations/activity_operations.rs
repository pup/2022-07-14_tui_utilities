pub trait ActivityOperations:Send + Sync + 'static
{
    fn get_activity
    (
        self:std::sync::Arc<Self>,
    ) -> std::sync::Arc<crate::utilities::activity::Activity>;
    fn any_activity
    (
        self:std::sync::Arc<Self>,
    ) -> std::sync::Arc<dyn std::any::Any + Send + Sync + 'static>;
    fn clear_activity
    (
        self:std::sync::Arc<Self>
    )
    {

    }
    fn activity_is_some
    (
        self:std::sync::Arc<Self>
    ) -> bool
    {
        true
    }
    fn set_active
    (
        self:std::sync::Arc<Self>, 
        value:bool
    )
    {
        *self.get_activity().active.lock().unwrap() = value;
    }
    fn is_active
    (
        self:std::sync::Arc<Self>
    ) -> bool
    {
        self.get_activity().active.lock().unwrap().clone()
    }
}
impl ActivityOperations for crate::utilities::activity::Activity
{
    fn get_activity
    (
        self:std::sync::Arc<Self>,
    ) -> std::sync::Arc<crate::utilities::activity::Activity> 
    {
        self.clone()    
    }
    fn any_activity
    (
        self:std::sync::Arc<Self>,
    ) -> std::sync::Arc<dyn std::any::Any + Send + Sync + 'static> 
    {
        self.clone()    
    }
}
impl Clone for crate::utilities::activity::Activity
{
    fn clone
    (
        &self
    ) -> crate::utilities::activity::Activity 
    {
        crate::utilities::activity::Activity
        {
            active:
            {
                std::sync::Mutex::new 
                (
                    self.active.lock().unwrap().clone()
                )
            }
        }
    }
}