use super::position_operations::PositionOperations;

pub trait BoundsOperations
{
    fn get_bounds
    (
        self:std::sync::Arc<Self>,
    ) -> std::sync::Arc<crate::utilities::bounds::Bounds>;
    fn any_bounds
    (
        self:std::sync::Arc<Self>,
    ) -> std::sync::Arc<dyn std::any::Any + Send + Sync + 'static>;
    fn clear_bounds
    (
        self:std::sync::Arc<Self>
    )
    {

    }
    fn bounds_is_some
    (
        self:std::sync::Arc<Self>
    ) -> bool
    {
        true
    }
    fn set_bounds
    (
        self:std::sync::Arc<Self>,
        bounds:&std::sync::Arc<crate::utilities::bounds::Bounds>
    )
    {
        *self.clone().get_bounds().horizontal_size.lock().unwrap() = bounds.horizontal_size.lock().unwrap().clone();
        *self.clone().get_bounds().vertical_size.lock().unwrap() = bounds.vertical_size.lock().unwrap().clone();
    }
    fn get_horizontal_bounds_size
    (
        self:std::sync::Arc<Self>
    ) -> usize
    {
        self.clone().get_bounds().horizontal_size.lock().unwrap().clone()
    }
    fn set_horizontal_bounds_size
    (
        self:std::sync::Arc<Self>, 
        value:usize
    )
    {
        *self.clone().get_bounds().horizontal_size.lock().unwrap() = value;
    }
    fn get_vertical_bounds_size
    (
        self:std::sync::Arc<Self>
    ) -> usize
    {
        self.clone().get_bounds().vertical_size.lock().unwrap().clone()
    }
    fn set_vertical_bounds_size
    (
        self:std::sync::Arc<Self>, 
        value:usize
    )
    {
        *self.clone().get_bounds().vertical_size.lock().unwrap() = value;
    }
    fn generate_horizontal_bounds_iter
    (
        self:std::sync::Arc<Self>
    ) -> Vec<usize>
    {
        (0..=self.clone().get_horizontal_bounds_size()).into_iter().collect::<Vec<usize>>()
    }
    fn generate_bounds_vertical_iter
    (
        self:std::sync::Arc<Self>
    ) -> Vec<usize>
    {
        (0..=self.clone().get_vertical_bounds_size()).into_iter().collect::<Vec<usize>>()
    }
    fn horizontal_bounds_size_not_equal_zero
    (
        self:std::sync::Arc<Self>
    ) -> bool
    {
        self.clone().get_horizontal_bounds_size() != 0
    }
    fn vertical_bounds_size_not_equal_zero
    (
        self:std::sync::Arc<Self>
    ) -> bool
    {
        self.clone().get_vertical_bounds_size() != 0
    }
    fn pointer_in_bounds
    (
        self:std::sync::Arc<Self>, 
        owner_position:&std::sync::Arc<crate::utilities::position::Position>,
        pointer_position:&std::sync::Arc<crate::utilities::position::Position>
    ) -> bool
    {
        owner_position.clone().get_x_position().unwrap() <= pointer_position.clone().get_x_position().unwrap() && 
        owner_position.clone().get_y_position().unwrap() <= pointer_position.clone().get_y_position().unwrap() ||
        owner_position.clone().get_x_position().unwrap() + self.clone().get_horizontal_bounds_size() >= pointer_position.clone().get_x_position().unwrap() && 
        owner_position.clone().get_y_position().unwrap() + self.clone().get_vertical_bounds_size() >= pointer_position.clone().get_y_position().unwrap()
    }
    fn pointer_on_bounds_left
    (
        self:std::sync::Arc<Self>, 
        owner_position:&std::sync::Arc<crate::utilities::position::Position>, 
        pointer_position:&std::sync::Arc<crate::utilities::position::Position>
    ) -> bool
    {
        owner_position.clone().get_x_position().unwrap() == pointer_position.clone().get_x_position().unwrap() &&
        owner_position.clone().get_y_position().unwrap() + self.clone().get_vertical_bounds_size() >= pointer_position.clone().get_y_position().unwrap()
    }
    fn pointer_on_bounds_right
    (
        self:std::sync::Arc<Self>, 
        owner_position:&std::sync::Arc<crate::utilities::position::Position>, 
        pointer_position:&std::sync::Arc<crate::utilities::position::Position>
    ) -> bool
    {
        owner_position.clone().get_x_position().unwrap() + self.clone().get_horizontal_bounds_size() == pointer_position.clone().get_x_position().unwrap() && 
        owner_position.clone().get_y_position().unwrap() + self.clone().get_vertical_bounds_size() >= pointer_position.clone().get_y_position().unwrap()
    }
    fn pointer_on_bounds_top
    (
        self:std::sync::Arc<Self>, 
        owner_position:&std::sync::Arc<crate::utilities::position::Position>, 
        pointer_position:&std::sync::Arc<crate::utilities::position::Position>
    ) -> bool
    {
        owner_position.clone().get_y_position().unwrap() == pointer_position.clone().get_y_position().unwrap() &&
        owner_position.clone().get_x_position().unwrap() + self.clone().get_horizontal_bounds_size() >= pointer_position.clone().get_x_position().unwrap()
    }
    fn pointer_on_bounds_bottom
    (
        self:std::sync::Arc<Self>, 
        owner_position:&std::sync::Arc<crate::utilities::position::Position>, 
        pointer_position:&std::sync::Arc<crate::utilities::position::Position>
    ) -> bool
    {
        owner_position.clone().get_y_position().unwrap() + self.clone().get_vertical_bounds_size() == pointer_position.clone().get_y_position().unwrap() &&
        owner_position.clone().get_x_position().unwrap() >= pointer_position.clone().get_x_position().unwrap()
    }
    fn calculate_pointer_bounds_distance
    (
        self:std::sync::Arc<Self>, 
        owner_position:&std::sync::Arc<crate::utilities::position::Position>, 
        pointer_position:&std::sync::Arc<crate::utilities::position::Position>
    ) -> Option<(Option<usize>, Option<usize>)>
    {
        if !self.pointer_in_bounds(owner_position, pointer_position) {return None;}
        let temp = owner_position.clone().positions_have_x_position_and_y_position(vec![pointer_position]);
        if  temp
        {
            Some((Some(owner_position.clone().get_x_position().unwrap() + pointer_position.clone().get_x_position().unwrap()), None))
        }
        else if temp
        {
            Some((None, Some(owner_position.clone().get_y_position().unwrap() + pointer_position.clone().get_y_position().unwrap())))
        }
        else if temp
        {
            Some((Some(owner_position.clone().get_x_position().unwrap() + pointer_position.clone().get_x_position().unwrap()), Some(owner_position.clone().get_y_position().unwrap() + pointer_position.clone().get_y_position().unwrap())))
        }
        else
        {
            None
        }
    }
    fn horizontal_bounds_greater_than_length
    (
        self:std::sync::Arc<Self>,
        length:usize
    ) -> bool
    {
        length < self.clone().get_horizontal_bounds_size()
    }
}
impl BoundsOperations for crate::utilities::bounds::Bounds
{
    fn get_bounds
    (
        self:std::sync::Arc<Self>,
    ) -> std::sync::Arc<crate::utilities::bounds::Bounds> 
    {
        self.clone()
    }
    fn any_bounds
    (
        self:std::sync::Arc<Self>,
    ) -> std::sync::Arc<dyn std::any::Any + Send + Sync + 'static> 
    {
        self.clone()
    }
}
impl Clone for crate::utilities::bounds::Bounds
{
    fn clone
    (
        &self
    ) -> crate::utilities::bounds::Bounds 
    {
        crate::utilities::bounds::Bounds
            {
                horizontal_size:
                std::sync::Mutex::new
                (
                    self.horizontal_size.lock().unwrap().clone()
                ),
                vertical_size:
                std::sync::Mutex::new
                (
                    self.vertical_size.lock().unwrap().clone()
                ),
            }
    }
}