use super::{activity_operations::ActivityOperations, bounds_operations::BoundsOperations, position_operations::PositionOperations};
use thread_manager::material_id_operations::MaterialIdOperations;
pub trait ComponentOperations
{
    fn get_component
    (
        self:std::sync::Arc<Self>
    ) -> std::sync::Arc<crate::utilities::component::Component>;
    fn any_component
    (
        self:std::sync::Arc<Self>
    ) -> std::sync::Arc<dyn std::any::Any + Send + Sync + 'static>;
    fn clear_component
    (
        self:std::sync::Arc<Self>
    )
    {

    }
    fn component_is_some
    (
        self:std::sync::Arc<Self>
    ) -> bool
    {
        true
    }
    fn get_component_metadata
    (
        self:std::sync::Arc<Self>
    ) -> std::sync::Arc<crate::utilities::component::Component>
    {
        std::sync::Arc::new 
        (
            crate::utilities::component::Component
            { 
                material_id:self.clone().get_component().material_id.clone(),
                component_index:
                std::sync::Mutex::new 
                (
                    self.clone().get_component().component_index.lock().unwrap().clone()
                ), 
                type_name:
                std::sync::Mutex::new 
                (
                    self.clone().get_component().type_name.lock().unwrap().clone()
                ),
                position:None,
                bounds:None,
                activity:self.clone().get_component().activity.clone()
            }
        )
    }
    fn set_component
    (
        self:std::sync::Arc<Self>,
        component:&std::sync::Arc<crate::utilities::component::Component>
    )
    {
        self.clone().get_component().set_material_id(component.clone().get_component().get_material_id());
        *self.clone().get_component().component_index.lock().unwrap() = component.clone().get_component().component_index.lock().unwrap().clone();
        *self.clone().get_component().type_name.lock().unwrap() = component.clone().get_component().type_name.lock().unwrap().clone();
        self.clone().get_component().get_position().set_position(&component.clone().get_component().get_position());
        self.clone().get_component().get_bounds().set_bounds(&component.clone().get_component().get_bounds());
    }
    fn set_component_index
    (
        self:std::sync::Arc<Self>,
        component_index:Option<usize>
    ) -> std::sync::Arc<Self>
    {
        *self.clone().get_component().component_index.lock().unwrap() = component_index;
        self.clone()
    }
    fn get_component_index
    (
        self:std::sync::Arc<Self>
    ) -> Option<usize>
    {
        self.clone().get_component().component_index.lock().unwrap().clone()
    }
    fn set_type_name
    (
        self:std::sync::Arc<Self>,
        type_name:Option<String>
    ) -> std::sync::Arc<Self>
    {
        *self.clone().get_component().type_name.lock().unwrap() = type_name;
        self.clone()
    }
    fn get_type_name
    (
        self:std::sync::Arc<Self>
    ) -> Option<String>
    {
        self.clone().get_component().type_name.lock().unwrap().clone()
    }
}
impl ComponentOperations for crate::utilities::component::Component
{
    fn get_component
    (
        self:std::sync::Arc<Self>
    ) -> std::sync::Arc<crate::utilities::component::Component> 
    {
        self.clone()    
    }
    fn any_component
    (
        self:std::sync::Arc<Self>
    ) -> std::sync::Arc<dyn std::any::Any + Send + Sync + 'static> 
    {
        self.clone()    
    }
}
impl crate::utilities_operations::position_operations::PositionOperations for crate::utilities::component::Component
{
    fn get_position
    (
        self:std::sync::Arc<Self>
    ) -> std::sync::Arc<crate::utilities::position::Position> 
    {
        self.position.as_ref().unwrap().clone()
    }
    fn any_position
    (
        self:std::sync::Arc<Self>
    ) -> std::sync::Arc<dyn std::any::Any + Send + Sync + 'static> 
    {
        self.clone()
    }
}
impl crate::utilities_operations::bounds_operations::BoundsOperations for crate::utilities::component::Component
{
    fn get_bounds
    (
        self:std::sync::Arc<Self>,
    ) -> std::sync::Arc<crate::utilities::bounds::Bounds> 
    {
        self.bounds.as_ref().unwrap().clone()
    }
    fn any_bounds
    (
        self:std::sync::Arc<Self>,
    ) -> std::sync::Arc<dyn std::any::Any + Send + Sync + 'static> 
    {
        self.clone()    
    }
}
impl crate::utilities_operations::activity_operations::ActivityOperations for crate::utilities::component::Component 
{
    fn get_activity
    (
        self:std::sync::Arc<Self>,
    ) -> std::sync::Arc<crate::utilities::activity::Activity> 
    {
        self.clone().activity.as_ref().unwrap().clone()    
    }
    fn any_activity
    (
        self:std::sync::Arc<Self>,
    ) -> std::sync::Arc<dyn std::any::Any + Send + Sync + 'static> 
    {
        self.clone()    
    }  
}
impl thread_manager::material_id_operations::MaterialIdOperations for crate::utilities::component::Component
{
    fn get_material_id
    (
        self:std::sync::Arc<Self>,
    ) -> std::sync::Arc<thread_manager::material_id::MaterialId> 
    {
        self.clone().material_id.as_ref().unwrap().clone()   
    }
    fn any_material_id
    (
        self:std::sync::Arc<Self>,
    ) -> std::sync::Arc<dyn std::any::Any + Send + Sync + 'static> 
    {
        self.clone()    
    }
}
impl Clone for crate::utilities::component::Component
{
    fn clone
    (
        &self
    ) -> crate::utilities::component::Component 
    {
        crate::utilities::component::Component
        { 
            material_id:Some(thread_manager::material_id::MaterialId::new_material_id(None, None)),
            component_index:
            std::sync::Mutex::new 
            (
                self.component_index.lock().unwrap().clone()
            ),
            type_name:
            std::sync::Mutex::new 
            (
                self.type_name.lock().unwrap().clone()
            ),
            position:
            Some
            (
                std::sync::Arc::new
                (
                    (**self.position.as_ref().unwrap()).clone()
                )
            ),
            bounds:
            Some
            (
                std::sync::Arc::new
                (
                    (**self.bounds.as_ref().unwrap()).clone()
                )
            ),
            activity:
            {
                let is_none = self.activity.is_none();
                let activity = self.activity.as_ref().unwrap().clone().is_active();
                if is_none
                {
                    None
                }
                else 
                {
                    Some(crate::utilities::activity::Activity::new_activity(Some(activity)))
                }
            }
        }
    }
}