pub trait ComponentSaveOperations: Send + Sync + 'static
{
    fn update
    (
        self:std::sync::Arc<Self>,
        component_save:&std::sync::Arc<crate::utilities::component_save::ComponentSave>,
        cursor:Option<&std::sync::Arc<crate::utilities_materials::cursor::Cursor>>
    );
    fn save
    (
        self:std::sync::Arc<Self>
    ) -> std::sync::Arc<crate::utilities::component_save::ComponentSave>;
}
impl Clone for crate::utilities::component_save::ComponentSave
{
    fn clone
    (
        &self
    ) -> crate::utilities::component_save::ComponentSave
    {
        crate::utilities::component_save::ComponentSave 
        { 
            menu_material_id: 
            std::sync::Mutex::new 
            (
                self.menu_material_id.lock().unwrap().clone()
            ),
            component_material_id: 
            std::sync::Mutex::new 
            (
                self.component_material_id.lock().unwrap().clone()
            ),
            component_type_name: 
            std::sync::Mutex::new 
            (
                self.component_type_name.lock().unwrap().clone()
            ),
            data: 
            {
                std::sync::Mutex::new 
                (
                    self.data.lock().unwrap().clone()
                )
            } 
        }
    }
}