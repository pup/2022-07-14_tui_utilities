use super::{bounds_operations::BoundsOperations, listing_bounds_operations::ListingBoundsOperations, position_operations::PositionOperations};

pub trait IndicesOperations
{
    fn get_indices
    (
        self:std::sync::Arc<Self>,
    ) -> std::sync::Arc<crate::utilities::indices::Indices>;
    fn any_indices
    (
        self:std::sync::Arc<Self>,
    ) -> std::sync::Arc<dyn std::any::Any + Send + Sync + 'static>;
    fn clear_indices
    (
        self:std::sync::Arc<Self>
    )
    {

    }
    fn indices_is_some
    (
        self:std::sync::Arc<Self>
    ) -> bool
    {
        true
    }
    fn update_indices
    (
        self:std::sync::Arc<Self>,
        owner_bounds:&std::sync::Arc<crate::utilities::bounds::Bounds>,
        owner_position:&std::sync::Arc<crate::utilities::position::Position>,
        owner_listing_bounds:&std::sync::Arc<crate::utilities::listing_bounds::ListingBounds>,
        pointer_position:&std::sync::Arc<crate::utilities::position::Position>
    )
    {
        if  owner_bounds.clone().pointer_in_bounds(owner_position, pointer_position) && 
            owner_listing_bounds.clone().horizontal_listing_bounds_is_some() && 
            owner_listing_bounds.clone().vertical_listing_bounds_is_some()
        {
            let horizontal = owner_listing_bounds.clone().get_listing_bounds_left().unwrap() + (owner_position.clone().get_x_position().unwrap() + owner_bounds.clone().get_horizontal_bounds_size() - pointer_position.clone().get_x_position().unwrap());
            let vertical = owner_listing_bounds.clone().get_listing_bounds_left().unwrap() + (owner_position.clone().get_y_position().unwrap() + owner_bounds.clone().get_vertical_bounds_size() - pointer_position.clone().get_y_position().unwrap());
            self.clone().set_vertical_indices_index(Some(vertical));
            self.clone().set_horizontal_indices_index(Some(horizontal));
        }
    }
    fn set_indices
    (
        self:&mut std::sync::Arc<Self>,
        indices:&std::sync::Arc<crate::utilities::indices::Indices>
    )
    {
        *self.clone().get_indices().vertical.lock().unwrap() = indices.clone().get_indices().vertical.lock().unwrap().clone();
        *self.clone().get_indices().horizontal.lock().unwrap() = indices.clone().get_indices().horizontal.lock().unwrap().clone();
    }
    fn set_vertical_indices_index
    (
        self:std::sync::Arc<Self>, 
        value:Option<usize>
    )
    {
        *self.clone().get_indices().vertical.lock().unwrap() = value;
    }
    fn get_vertical_indices_index
    (
        self:std::sync::Arc<Self>,
    ) -> Option<usize>
    {
        self.clone().get_indices().vertical.lock().unwrap().clone()
    }
    fn vertical_indices_is_some
    (
        self:std::sync::Arc<Self>,
    ) -> bool
    {
        self.clone().get_indices().vertical.lock().unwrap().is_some()
    }
    fn set_horizontal_indices_index
    (
        self:std::sync::Arc<Self>, 
        value:Option<usize>
    )
    {
        *self.clone().get_indices().horizontal.lock().unwrap() = value
    }
    fn get_horizontal_indices_index
    (
        self:std::sync::Arc<Self>,
    ) -> Option<usize>
    {
        self.clone().get_indices().horizontal.lock().unwrap().clone()
    }
    fn horizontal_indices_is_some
    (
        self:std::sync::Arc<Self>,
    ) -> bool
    {
        self.clone().get_indices().horizontal.lock().unwrap().is_some()
    }
    fn vertical_indices_equal_bounds_up
    (
        self:std::sync::Arc<Self>,
        bounds:&std::sync::Arc<crate::utilities::listing_bounds::ListingBounds>
    ) -> bool
    {
        if self.clone().get_indices().vertical_indices_is_some()
        {
            if bounds.clone().up_listing_bounds_is_some()
            {
                bounds.clone().get_listing_bounds_left() == self.clone().get_vertical_indices_index()
            }
            else
            {
               false 
            }
        }
        else
        {
            false
        }
    }
    fn vertical_indices_equal_bounds_down
    (
        self:std::sync::Arc<Self>,
        bounds:&std::sync::Arc<crate::utilities::listing_bounds::ListingBounds>
    ) -> bool
    {
        if self.clone().get_indices().vertical_indices_is_some()
        {
            if bounds.clone().down_listing_bounds_is_some()
            {
                bounds.clone().get_listing_bounds_down() == self.clone().get_vertical_indices_index()
            }
            else
            {
               false 
            }
        }
        else
        {
            false
        }
    }
    fn horizontal_indices_equal_bounds_left
    (
        self:std::sync::Arc<Self>,
        bounds:&std::sync::Arc<crate::utilities::listing_bounds::ListingBounds>
    ) -> bool
    {
        if self.clone().get_indices().horizontal_indices_is_some()
        {
            if bounds.clone().left_listing_bounds_is_some()
            {
                bounds.clone().get_listing_bounds_left() == self.clone().get_horizontal_indices_index()
            }
            else
            {
               false 
            }
        }
        else
        {
            false
        }
    }
    fn horizontal_indices_equal_bounds_right
    (
        self:std::sync::Arc<Self>,
        bounds:&std::sync::Arc<crate::utilities::listing_bounds::ListingBounds>
    ) -> bool
    {
        if self.clone().get_indices().horizontal_indices_is_some()
        {
            if bounds.clone().right_listing_bounds_is_some()
            {
                bounds.clone().get_listing_bounds_right() == self.clone().get_horizontal_indices_index()
            }
            else
            {
               false 
            }
        }
        else
        {
            false
        }
    }
    fn increment_horizontal_indices
    (
        self:std::sync::Arc<Self>, 
        value:usize
    )
    {
        self.clone().set_horizontal_indices_index(Some(self.clone().get_horizontal_indices_index().unwrap() + value))
    }
    fn decrement_horizontal_indices
    (
        self:std::sync::Arc<Self>, 
        value:usize
    )
    {
        self.clone().set_horizontal_indices_index(Some(self.clone().get_horizontal_indices_index().unwrap() - value))
    }
    fn increment_vertical_indices
    (
        self:std::sync::Arc<Self>, 
        value:usize
    )
    {
        self.clone().set_vertical_indices_index(Some(self.clone().get_vertical_indices_index().unwrap() + value))
    }
    fn decrement_vertical_indices
    (
        self:std::sync::Arc<Self>, 
        value:usize
    )
    {
        self.clone().set_vertical_indices_index(Some(self.clone().get_vertical_indices_index().unwrap() - value))
    }
    fn indices_equal
    (
        self:std::sync::Arc<Self>,
        comparator:&std::sync::Arc<crate::utilities::indices::Indices>
    ) -> bool
    {
        self.clone().get_vertical_indices_index().unwrap() == comparator.clone().get_vertical_indices_index().unwrap() &&
        self.clone().get_horizontal_indices_index().unwrap() == comparator.clone().get_horizontal_indices_index().unwrap()
    }
    fn vertical_indices_equal
    (
        self:std::sync::Arc<Self>,
        comparator:&std::sync::Arc<crate::utilities::indices::Indices>
    ) -> bool
    {
        self.clone().get_vertical_indices_index().unwrap() == comparator.clone().get_vertical_indices_index().unwrap()
    }
    fn horizontal_indices_equal
    (
        self:std::sync::Arc<Self>,
        comparator:&std::sync::Arc<crate::utilities::indices::Indices>
    ) -> bool
    {
        self.clone().get_horizontal_indices_index().unwrap() == comparator.clone().get_horizontal_indices_index().unwrap()
    }
    fn indices_not_equal
    (
        self:std::sync::Arc<Self>,
        comparator:&std::sync::Arc<crate::utilities::indices::Indices>
    ) -> bool
    {
        !(self.clone().get_vertical_indices_index().unwrap() == comparator.clone().get_vertical_indices_index().unwrap() &&
        self.clone().get_horizontal_indices_index().unwrap() == comparator.clone().get_horizontal_indices_index().unwrap())
    }
    fn vertical_indices_not_equal
    (
        self:std::sync::Arc<Self>,
        comparator:&std::sync::Arc<crate::utilities::indices::Indices>
    ) -> bool
    {
        !(self.clone().get_vertical_indices_index().unwrap() == comparator.clone().get_vertical_indices_index().unwrap())
    }
    fn horizontal_indices_not_equal
    (
        self:std::sync::Arc<Self>,
        comparator:&std::sync::Arc<crate::utilities::indices::Indices>
    ) -> bool
    {
        !(self.clone().get_horizontal_indices_index().unwrap() == comparator.clone().get_horizontal_indices_index().unwrap())
    }
    fn indices_less_than
    (
        self:std::sync::Arc<Self>,
        comparator:&std::sync::Arc<crate::utilities::indices::Indices>
    ) -> bool
    {
        self.clone().get_vertical_indices_index().unwrap() < comparator.clone().get_vertical_indices_index().unwrap() &&
        self.clone().get_horizontal_indices_index().unwrap() < comparator.clone().get_horizontal_indices_index().unwrap()
    }
    fn vertical_indices_less_than
    (
        self:std::sync::Arc<Self>,
        comparator:&std::sync::Arc<crate::utilities::indices::Indices>
    ) -> bool
    {
        self.clone().get_vertical_indices_index().unwrap() < comparator.clone().get_vertical_indices_index().unwrap()
    }
    fn horizontal_indices_less_than
    (
        self:std::sync::Arc<Self>,
        comparator:&std::sync::Arc<crate::utilities::indices::Indices>
    ) -> bool
    {
        self.clone().get_horizontal_indices_index().unwrap() < comparator.clone().get_horizontal_indices_index().unwrap()
    }
    fn indices_greater_than
    (
        self:std::sync::Arc<Self>,
        comparator:&std::sync::Arc<crate::utilities::indices::Indices>
    ) -> bool
    {
        self.clone().get_vertical_indices_index().unwrap() > comparator.clone().get_vertical_indices_index().unwrap() &&
        self.clone().get_horizontal_indices_index().unwrap() > comparator.clone().get_horizontal_indices_index().unwrap()
    }
    fn vertical_indices_greater_than
    (
        self:std::sync::Arc<Self>,
        comparator:&std::sync::Arc<crate::utilities::indices::Indices>
    ) -> bool
    {
        self.clone().get_vertical_indices_index().unwrap() > comparator.clone().get_vertical_indices_index().unwrap() 
    }
    fn horizontal_indices_greater_than
    (
        self:std::sync::Arc<Self>,
        comparator:&std::sync::Arc<crate::utilities::indices::Indices>
    ) -> bool
    {
        self.clone().get_horizontal_indices_index().unwrap() > comparator.clone().get_horizontal_indices_index().unwrap()
    }
    fn indices_less_than_equal_to
    (
        self:std::sync::Arc<Self>,
        comparator:&std::sync::Arc<crate::utilities::indices::Indices>
    ) -> bool
    {
        self.clone().get_vertical_indices_index().unwrap() <= comparator.clone().get_vertical_indices_index().unwrap() &&
        self.clone().get_horizontal_indices_index().unwrap() <= comparator.clone().get_horizontal_indices_index().unwrap()
    }
    fn vertical_indices_less_than_equal_to
    (
        self:std::sync::Arc<Self>,
        comparator:&std::sync::Arc<crate::utilities::indices::Indices>
    ) -> bool
    {
        self.clone().get_vertical_indices_index().unwrap() <= comparator.clone().get_vertical_indices_index().unwrap() 
    }
    fn horizontal_indices_less_than_equal_to
    (
        self:std::sync::Arc<Self>,
        comparator:&std::sync::Arc<crate::utilities::indices::Indices>
    ) -> bool
    {
        self.clone().get_horizontal_indices_index().unwrap() <= comparator.clone().get_horizontal_indices_index().unwrap()
    }
    fn indices_greater_than_equal_to
    (
        self:std::sync::Arc<Self>,
        comparator:&std::sync::Arc<crate::utilities::indices::Indices>
    ) -> bool
    {
        self.clone().get_vertical_indices_index().unwrap() >= comparator.clone().get_vertical_indices_index().unwrap() &&
        self.clone().get_horizontal_indices_index().unwrap() >= comparator.clone().get_horizontal_indices_index().unwrap()
    }
    fn vertical_indices_greater_than_equal_to
    (
        self:std::sync::Arc<Self>,
        comparator:&std::sync::Arc<crate::utilities::indices::Indices>
    ) -> bool
    {
        self.clone().get_vertical_indices_index().unwrap() >= comparator.clone().get_vertical_indices_index().unwrap() 
    }
    fn horizontal_indices_greater_than_equal_to
    (
        self:std::sync::Arc<Self>,
        comparator:&std::sync::Arc<crate::utilities::indices::Indices>
    ) -> bool
    {
        self.clone().get_horizontal_indices_index().unwrap() >= comparator.clone().get_horizontal_indices_index().unwrap()
    }
}
impl IndicesOperations for crate::utilities::indices::Indices
{
    fn get_indices
    (
        self:std::sync::Arc<Self>,
    ) -> std::sync::Arc<crate::utilities::indices::Indices> 
    {
        self.clone()  
    }
    fn any_indices
    (
        self:std::sync::Arc<Self>,
    ) -> std::sync::Arc<dyn std::any::Any + Send + Sync + 'static> 
    {
        self.clone()    
    }
}
impl Clone for crate::utilities::indices::Indices
{
    fn clone
    (
        &self
    ) -> crate::utilities::indices::Indices
    {
        crate::utilities::indices::Indices
        {
            vertical:
            std::sync::Mutex::new 
            (
                self.vertical.lock().unwrap().clone()
            ),
            horizontal:
            std::sync::Mutex::new 
            (
                self.horizontal.lock().unwrap().clone()
            ),
        }
    }
}