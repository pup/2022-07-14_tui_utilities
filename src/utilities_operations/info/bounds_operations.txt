A global operator that allows structures to get their
bounding information if they have the available
variable definitions.