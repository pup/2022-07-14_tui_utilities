A global operator that allows structures to get their
position information if they have the available
variable definition.