pub trait KeyOperations
{
    fn is_up
    (
        &self
    ) -> bool;
    fn is_down
    (
        &self
    ) -> bool;
    fn is_left
    (
        &self
    ) -> bool;
    fn is_right
    (
        &self
    ) -> bool;
    fn is_save
    (
        &self
    ) -> bool;
    fn is_backspace
    (
        &self
    ) -> bool;
    fn is_add
    (
        &self
    ) -> bool;
    fn is_character
    (
        &self
    ) -> bool;
    fn get_character
    (
        &self
    ) -> Option<char>;
}
impl KeyOperations for termion::event::Event
{
    fn is_up
    (
        &self
    ) -> bool
    {
        *self == termion::event::Event::Key(termion::event::Key::Up)
    }
    fn is_down
    (
        &self
    ) -> bool
    {
        *self == termion::event::Event::Key(termion::event::Key::Down)
    }
    fn is_left
    (
        &self
    ) -> bool
    {
        *self == termion::event::Event::Key(termion::event::Key::Left)
    }
    fn is_right
    (
        &self
    ) -> bool
    {
        *self == termion::event::Event::Key(termion::event::Key::Right)
    }
    fn is_save
    (
        &self
    ) -> bool
    {
        *self == termion::event::Event::Key(termion::event::Key::Insert)
    }
    fn is_backspace
    (
        &self
    ) -> bool 
    {
        *self == termion::event::Event::Key(termion::event::Key::Backspace)
    }
    fn is_add
    (
        &self
    ) -> bool
    {
        *self == termion::event::Event::Key(termion::event::Key::Ctrl('s'))
    }
    fn is_character
    (
        &self
    ) -> bool
    {
        match self 
        {
            termion::event::Event::Key(char) => 
            {
                std::mem::discriminant(char) == std::mem::discriminant(&termion::event::Key::Char(' '))
            },
            _ => 
            {
                false
            }
        }
    }
    fn get_character
    (
        &self
    ) -> Option<char>
    {
        match self 
        {
            termion::event::Event::Key(char) => 
            {
                if let termion::event::Key::Char(character) = char
                {
                    Some
                    (
                        character.clone()
                    )
                }
                else
                {
                    None
                }
            },
            _ => 
            {
                None
            }
        }
    }
}