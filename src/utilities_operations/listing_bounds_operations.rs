use super::indices_operations::IndicesOperations;

pub trait ListingBoundsOperations
{
    fn get_listing_bounds
    (
        self:std::sync::Arc<Self>
    ) -> std::sync::Arc<crate::utilities::listing_bounds::ListingBounds>;
    fn any_list_bounds
    (
        self:std::sync::Arc<Self>
    ) -> std::sync::Arc<dyn std::any::Any + Send + Sync + 'static>;
    fn clear_listing_bounds
    (
        self:std::sync::Arc<Self>
    )
    {

    }
    fn listing_bounds_is_some
    (
        self:std::sync::Arc<Self>
    ) -> bool
    {
        true
    }
    fn set_listing_bounds
    (
        self:std::sync::Arc<Self>,
        listing_bounds:std::sync::Arc<crate::utilities::listing_bounds::ListingBounds>
    )
    {
        *self.clone().get_listing_bounds().buffer.lock().unwrap() = listing_bounds.clone().get_listing_bounds().buffer.lock().unwrap().clone();
    }
    fn set_listing_bounds_left
    (
        self:std::sync::Arc<Self>, 
        left:Option<usize>
    )
    {
        self.clone().clone().get_listing_bounds().buffer.lock().unwrap()[0] = left;
    }
    fn get_listing_bounds_left
    (
        self:std::sync::Arc<Self>,
    ) -> Option<usize>
    {
        self.clone().clone().get_listing_bounds().buffer.lock().unwrap()[0].clone()
    }
    fn set_listing_bounds_right(
        self:std::sync::Arc<Self>, 
        right:Option<usize>
    )
    {
        self.clone().clone().get_listing_bounds().buffer.lock().unwrap()[1] = right;
    }
    fn get_listing_bounds_right
    (
        self:std::sync::Arc<Self>,
    ) -> Option<usize>
    {
        self.clone().clone().get_listing_bounds().buffer.lock().unwrap()[1].clone()
    }
    fn set_listing_bounds_up
    (
        self:std::sync::Arc<Self>, 
        up:Option<usize>
    )
    {
        self.clone().clone().get_listing_bounds().buffer.lock().unwrap()[2] = up;
    }
    fn get_listing_bounds_up
    (
        self:std::sync::Arc<Self>,
    ) -> Option<usize>
    {
        self.clone().clone().get_listing_bounds().buffer.lock().unwrap()[2].clone()
    }
    fn set_listing_bounds_down
    (
        self:std::sync::Arc<Self>, 
        down:Option<usize>
    )
    {
        self.clone().clone().get_listing_bounds().buffer.lock().unwrap()[3] = down;
    }
    fn get_listing_bounds_down
    (
        self:std::sync::Arc<Self>,
    ) -> Option<usize>
    {
        self.clone().clone().get_listing_bounds().buffer.lock().unwrap()[3].clone()
    }
    fn generate_horizontal_iter
    (
        self:std::sync::Arc<Self>,
    ) -> Option<Vec<usize>>
    {
        if self.clone().left_listing_bounds_is_some() && self.clone().right_listing_bounds_is_some() {return None}
        Some
        (
            (self.clone().get_listing_bounds_left().unwrap()..=self.clone().get_listing_bounds_right().unwrap()).into_iter().collect::<Vec<usize>>()
        )
    }
    fn generate_vertical_iter
    (
        self:std::sync::Arc<Self>,
    ) -> Option<Vec<usize>>
    {
        if self.clone().up_listing_bounds_is_some() && self.clone().down_listing_bounds_is_some() {return None}
        Some((self.clone().get_listing_bounds_up().unwrap()..=self.clone().get_listing_bounds_down().unwrap()).into_iter().collect::<Vec<usize>>())
    }
    fn left_listing_bounds_is_some
    (
        self:std::sync::Arc<Self>,
    ) -> bool
    {
        self.clone().clone().get_listing_bounds().buffer.lock().unwrap()[0].is_some()
    }
    fn right_listing_bounds_is_some
    (
        self:std::sync::Arc<Self>,
    ) -> bool
    {
        self.clone().clone().get_listing_bounds().buffer.lock().unwrap()[1].is_some()
    }
    fn up_listing_bounds_is_some
    (
        self:std::sync::Arc<Self>,
    ) -> bool
    {
        self.clone().clone().get_listing_bounds().buffer.lock().unwrap()[2].is_some()
    }
    fn down_listing_bounds_is_some
    (
        self:std::sync::Arc<Self>,
    ) -> bool
    {
        self.clone().clone().get_listing_bounds().buffer.lock().unwrap()[3].is_some()
    }
    fn set_listing_bounds_buffer
    (
        self:std::sync::Arc<Self>, 
        bounds:Vec<Option<usize>>
    )
    {
        *self.clone().clone().get_listing_bounds().buffer.lock().unwrap() = bounds.into_iter().map(|side| {side}).collect();
    }
    fn get_listing_bounds_buffer
    (
        self:std::sync::Arc<Self>,
    ) -> Vec<Option<usize>>
    {
        self.clone().clone().get_listing_bounds().buffer.lock().unwrap().clone()
    }
    fn horizontal_listing_bounds_is_some
    (
        self:std::sync::Arc<Self>,
    ) -> bool
    {
        self.clone().clone().get_listing_bounds().buffer.lock().unwrap()[0].is_some() && self.clone().clone().get_listing_bounds().buffer.lock().unwrap()[1].is_some()  
    }
    fn vertical_listing_bounds_is_some
    (
        self:std::sync::Arc<Self>,
    ) -> bool
    {
        self.clone().clone().get_listing_bounds().buffer.lock().unwrap()[2].is_some() && self.clone().clone().get_listing_bounds().buffer.lock().unwrap()[3].is_some()
    }
    fn index_in_horizontal_listing_bounds
    (
        self:std::sync::Arc<Self>,
        index:usize
    ) -> bool
    {
        self.clone().get_listing_bounds_left().is_some() &&
        self.clone().get_listing_bounds_right().is_some() &&
        self.clone().get_listing_bounds_left().unwrap() < index && 
        self.clone().get_listing_bounds_right().unwrap() > index
    }
    fn index_less_than_horizontal_listing_bounds
    (
        self:std::sync::Arc<Self>,
        index:usize
    ) -> bool
    {
        self.clone().get_listing_bounds_left().is_some() &&
        self.clone().get_listing_bounds_right().is_some() &&
        self.clone().get_listing_bounds_left().unwrap() > index && 
        self.clone().get_listing_bounds_right().unwrap() > index
    }
    fn index_greater_than_horizontal_listing_bounds
    (
        self:std::sync::Arc<Self>,
        index:usize
    ) -> bool
    {
        self.clone().get_listing_bounds_left().is_some() &&
        self.clone().get_listing_bounds_right().is_some() &&
        self.clone().get_listing_bounds_left().unwrap() < index && 
        self.clone().get_listing_bounds_right().unwrap() < index
    }
    fn index_in_vertical_listing_bounds
    (
        self:std::sync::Arc<Self>,
        index:usize
    ) -> bool
    {
        self.clone().get_listing_bounds_up().is_some() &&
        self.clone().get_listing_bounds_down().is_some() &&
        self.clone().get_listing_bounds_up().unwrap() < index && 
        self.clone().get_listing_bounds_down().unwrap() > index
    }
    fn index_less_than_vertical_listing_bounds
    (
        self:std::sync::Arc<Self>,
        index:usize
    ) -> bool
    {
        self.clone().get_listing_bounds_up().is_some() &&
        self.clone().get_listing_bounds_down().is_some() &&
        self.clone().get_listing_bounds_up().unwrap() > index && 
        self.clone().get_listing_bounds_down().unwrap() > index
    }
    fn index_greater_than_vertical_listing_bounds
    (
        self:std::sync::Arc<Self>,
        index:usize
    ) -> bool
    {
        self.clone().get_listing_bounds_up().is_some() &&
        self.clone().get_listing_bounds_down().is_some() &&
        self.clone().get_listing_bounds_up().unwrap() < index && 
        self.clone().get_listing_bounds_down().unwrap() < index
    }
    fn get_horizontal_listing_bounds_size
    (
        self:std::sync::Arc<Self>,
    ) -> usize
    {
        self.clone().clone().get_listing_bounds().buffer.lock().unwrap()[1].unwrap() - self.clone().clone().get_listing_bounds().buffer.lock().unwrap()[0].unwrap()
    }
    fn get_vertical_listing_bounds_size
    (
        self:std::sync::Arc<Self>,
    ) -> usize
    {
        self.clone().clone().get_listing_bounds().buffer.lock().unwrap()[3].unwrap() - self.clone().clone().get_listing_bounds().buffer.lock().unwrap()[2].unwrap()
    }
    fn increment_listing_bounds_left
    (
        self:std::sync::Arc<Self>, 
        value:usize
    )
    {
        self.clone().clone().get_listing_bounds().buffer.lock().unwrap()[0] = Some(self.clone().clone().get_listing_bounds().buffer.lock().unwrap()[0].unwrap() + value)
    }
    fn decrement_listing_bounds_left
    (
        self:std::sync::Arc<Self>, 
        value:usize
    )
    {
        self.clone().clone().get_listing_bounds().buffer.lock().unwrap()[0] = Some(self.clone().clone().get_listing_bounds().buffer.lock().unwrap()[0].unwrap() - value)
    }
    fn increment_listing_bounds_right
    (
        self:std::sync::Arc<Self>, 
        value:usize
    )
    {
        self.clone().clone().get_listing_bounds().buffer.lock().unwrap()[1] = Some(self.clone().clone().get_listing_bounds().buffer.lock().unwrap()[1].unwrap() + value)
    }
    fn decrement_listing_bounds_right
    (
        self:std::sync::Arc<Self>, 
        value:usize
    )
    {
        self.clone().clone().get_listing_bounds().buffer.lock().unwrap()[1] = Some(self.clone().clone().get_listing_bounds().buffer.lock().unwrap()[1].unwrap() - value)
    }
    fn increment_listing_bounds_up
    (
        self:std::sync::Arc<Self>, 
        value:usize
    )
    {
        self.clone().clone().get_listing_bounds().buffer.lock().unwrap()[2] = Some(self.clone().clone().get_listing_bounds().buffer.lock().unwrap()[2].unwrap() + value)
    }
    fn decrement_listing_bounds_up
    (
        self:std::sync::Arc<Self>, 
        value:usize
    )
    {
        self.clone().clone().get_listing_bounds().buffer.lock().unwrap()[2] = Some(self.clone().clone().get_listing_bounds().buffer.lock().unwrap()[2].unwrap() - value)
    }
    fn increment_listing_bounds_down
    (
        self:std::sync::Arc<Self>, 
        value:usize
    )
    {
        self.clone().clone().get_listing_bounds().buffer.lock().unwrap()[3] = Some(self.clone().clone().get_listing_bounds().buffer.lock().unwrap()[3].unwrap() + value)
    }
    fn decrement_listing_bounds_down
    (
        self:std::sync::Arc<Self>, 
        value:usize
    )
    {
        self.clone().clone().get_listing_bounds().buffer.lock().unwrap()[3] = Some(self.clone().clone().get_listing_bounds().buffer.lock().unwrap()[3].unwrap() - value)
    }
    fn move_listing_bounds_left
    (
        self:std::sync::Arc<Self>, 
        value:usize
    )
    {
        if self.clone().get_listing_bounds_left().unwrap() != 0 || (self.clone().get_listing_bounds_left().unwrap()  as isize - value as isize) > 0 as isize
        {
            self.clone().decrement_listing_bounds_left(value);
            self.clone().decrement_listing_bounds_right(value);
        }
    }
    fn move_listing_bounds_right
    (
        self:std::sync::Arc<Self>, 
        subject:usize, 
        value:usize
    )
    {
        if self.clone().get_listing_bounds_right().unwrap() < value
        {
            self.clone().increment_listing_bounds_left(subject);
            self.clone().increment_listing_bounds_right(subject);
        }
    }
    fn move_listing_bounds_down
    (
        self:std::sync::Arc<Self>, 
        subject:usize, 
        value:usize
    )
    {
        if self.clone().get_listing_bounds_down().unwrap() < value
        {
            self.clone().increment_listing_bounds_up(subject);
            self.clone().increment_listing_bounds_down(subject);
        }
    }
    fn move_listing_bounds_up
    (
        self:std::sync::Arc<Self>, 
        value:usize
    )
    {
        if self.clone().get_listing_bounds_up().unwrap() != 0 || (self.clone().get_listing_bounds_up().unwrap()  as isize - value as isize) > 0 as isize
        {
            self.clone().decrement_listing_bounds_up(value);
            self.clone().decrement_listing_bounds_down(value);
        }
    }
    fn horizontal_listing_bounds_move_to
    (
        self:std::sync::Arc<Self>, 
        horizontal:Option<usize>
    )
    {
        if horizontal.is_some()
        {
            let horizontal_size = self.clone().get_horizontal_listing_bounds_size();
            self.clone().set_listing_bounds_right(horizontal);
            self.clone().set_listing_bounds_left(Some(horizontal.clone().unwrap() - horizontal_size))
        }
    }
    fn vertical_listing_bounds_move_to
    (
        self:std::sync::Arc<Self>, 
        vertical:Option<usize>
    )
    {
        if vertical.is_some()
        {
            let vertical_size = self.clone().get_horizontal_listing_bounds_size();
            self.clone().set_listing_bounds_right(vertical);
            self.clone().set_listing_bounds_left(Some(vertical.clone().unwrap() - vertical_size))
        }
    }
    fn right_equal_length_and_x_index
    (
        self:std::sync::Arc<Self>,
        length:usize,
        indices:std::sync::Arc<crate::utilities::indices::Indices>
    ) -> bool
    {
        self.clone().get_listing_bounds_right().unwrap() == length - 1 && 
        length - 1 == indices.get_horizontal_indices_index().unwrap()
    }
    fn down_equal_length_and_y_index
    (
        self:std::sync::Arc<Self>,
        length:usize,
        indices:std::sync::Arc<crate::utilities::indices::Indices>
    ) -> bool
    {
        self.clone().get_listing_bounds_left().unwrap()  == length && 
        length == indices.get_vertical_indices_index().unwrap()
    }
    fn listing_bounds_equal
    (
        self:std::sync::Arc<Self>,
        comparator:&std::sync::Arc<crate::utilities::listing_bounds::ListingBounds>,
    ) -> bool
    {
        self.clone().clone().get_listing_bounds().buffer.lock().unwrap()[0] == comparator.clone().get_listing_bounds().buffer.lock().unwrap()[0] && 
        self.clone().clone().get_listing_bounds().buffer.lock().unwrap()[1] == comparator.clone().get_listing_bounds().buffer.lock().unwrap()[1] && 
        self.clone().clone().get_listing_bounds().buffer.lock().unwrap()[2] == comparator.clone().get_listing_bounds().buffer.lock().unwrap()[2] && 
        self.clone().clone().get_listing_bounds().buffer.lock().unwrap()[3] == comparator.clone().get_listing_bounds().buffer.lock().unwrap()[3]
    }
    fn horizontal_listing_bounds_equal
    (
        self:std::sync::Arc<Self>,
        comparator:&std::sync::Arc<crate::utilities::listing_bounds::ListingBounds>,
    ) -> bool
    {
        self.clone().clone().get_listing_bounds().buffer.lock().unwrap()[0] == comparator.clone().get_listing_bounds().buffer.lock().unwrap()[0] && 
        self.clone().clone().get_listing_bounds().buffer.lock().unwrap()[1] == comparator.clone().get_listing_bounds().buffer.lock().unwrap()[1]
    }
    fn vertical_listing_bounds_equal
    (
        self:std::sync::Arc<Self>,
        comparator:&std::sync::Arc<crate::utilities::listing_bounds::ListingBounds>,
    ) -> bool
    {
        self.clone().clone().get_listing_bounds().buffer.lock().unwrap()[2] == comparator.clone().get_listing_bounds().buffer.lock().unwrap()[2] && 
        self.clone().clone().get_listing_bounds().buffer.lock().unwrap()[3] == comparator.clone().get_listing_bounds().buffer.lock().unwrap()[3]
    }
    fn listing_bounds_not_equal
    (
        self:std::sync::Arc<Self>,
        comparator:&std::sync::Arc<crate::utilities::listing_bounds::ListingBounds>,
    ) -> bool
    {
      !(self.clone().clone().get_listing_bounds().buffer.lock().unwrap()[0] == comparator.clone().get_listing_bounds().buffer.lock().unwrap()[0] && 
        self.clone().clone().get_listing_bounds().buffer.lock().unwrap()[1] == comparator.clone().get_listing_bounds().buffer.lock().unwrap()[1] && 
        self.clone().clone().get_listing_bounds().buffer.lock().unwrap()[2] == comparator.clone().get_listing_bounds().buffer.lock().unwrap()[2] && 
        self.clone().clone().get_listing_bounds().buffer.lock().unwrap()[3] == comparator.clone().get_listing_bounds().buffer.lock().unwrap()[3])
    }
    fn horizontal_listing_bounds_not_equal
    (
        self:std::sync::Arc<Self>,
        comparator:&std::sync::Arc<crate::utilities::listing_bounds::ListingBounds>,
    ) -> bool
    {
      !(self.clone().clone().get_listing_bounds().buffer.lock().unwrap()[0] == comparator.clone().get_listing_bounds().buffer.lock().unwrap()[0] && 
        self.clone().clone().get_listing_bounds().buffer.lock().unwrap()[1] == comparator.clone().get_listing_bounds().buffer.lock().unwrap()[1])
    }
    fn vertical_listing_bounds_not_equal
    (
        self:std::sync::Arc<Self>,
        comparator:&std::sync::Arc<crate::utilities::listing_bounds::ListingBounds>,
    ) -> bool
    {
      !(self.clone().clone().get_listing_bounds().buffer.lock().unwrap()[2] == comparator.clone().get_listing_bounds().buffer.lock().unwrap()[2] && 
        self.clone().clone().get_listing_bounds().buffer.lock().unwrap()[3] == comparator.clone().get_listing_bounds().buffer.lock().unwrap()[3])
    }
    fn listing_bounds_less_than
    (
        self:std::sync::Arc<Self>,
        comparator:&std::sync::Arc<crate::utilities::listing_bounds::ListingBounds>,
    ) -> bool
    {
        self.clone().clone().get_listing_bounds().buffer.lock().unwrap()[0] < comparator.clone().get_listing_bounds().buffer.lock().unwrap()[0] &&
        self.clone().clone().get_listing_bounds().buffer.lock().unwrap()[1] < comparator.clone().get_listing_bounds().buffer.lock().unwrap()[1] &&
        self.clone().clone().get_listing_bounds().buffer.lock().unwrap()[2] < comparator.clone().get_listing_bounds().buffer.lock().unwrap()[2] &&
        self.clone().clone().get_listing_bounds().buffer.lock().unwrap()[3] < comparator.clone().get_listing_bounds().buffer.lock().unwrap()[3]
    }
    fn horizontal_listing_bounds_less_than
    (
        self:std::sync::Arc<Self>,
        comparator:&std::sync::Arc<crate::utilities::listing_bounds::ListingBounds>,
    ) -> bool
    {
        self.clone().clone().get_listing_bounds().buffer.lock().unwrap()[0] < comparator.clone().get_listing_bounds().buffer.lock().unwrap()[0] &&
        self.clone().clone().get_listing_bounds().buffer.lock().unwrap()[1] < comparator.clone().get_listing_bounds().buffer.lock().unwrap()[1]
    }
    fn vertical_listing_bounds_less_than
    (
        self:std::sync::Arc<Self>,
        comparator:&std::sync::Arc<crate::utilities::listing_bounds::ListingBounds>,
    ) -> bool
    {
        self.clone().clone().get_listing_bounds().buffer.lock().unwrap()[2] < comparator.clone().get_listing_bounds().buffer.lock().unwrap()[2] &&
        self.clone().clone().get_listing_bounds().buffer.lock().unwrap()[3] < comparator.clone().get_listing_bounds().buffer.lock().unwrap()[3]
    }
    fn listing_bounds_greater_than
    (
        self:std::sync::Arc<Self>,
        comparator:&std::sync::Arc<crate::utilities::listing_bounds::ListingBounds>,
    ) -> bool
    {
        self.clone().clone().get_listing_bounds().buffer.lock().unwrap()[0] > comparator.clone().get_listing_bounds().buffer.lock().unwrap()[0] &&
        self.clone().clone().get_listing_bounds().buffer.lock().unwrap()[1] > comparator.clone().get_listing_bounds().buffer.lock().unwrap()[1] &&
        self.clone().clone().get_listing_bounds().buffer.lock().unwrap()[2] > comparator.clone().get_listing_bounds().buffer.lock().unwrap()[2] &&
        self.clone().clone().get_listing_bounds().buffer.lock().unwrap()[3] > comparator.clone().get_listing_bounds().buffer.lock().unwrap()[3]
    }
    fn horizontal_listing_bounds_greater_than
    (
        self:std::sync::Arc<Self>,
        comparator:&std::sync::Arc<crate::utilities::listing_bounds::ListingBounds>,
    ) -> bool
    {
        self.clone().clone().get_listing_bounds().buffer.lock().unwrap()[0] < comparator.clone().get_listing_bounds().buffer.lock().unwrap()[0] &&
        self.clone().clone().get_listing_bounds().buffer.lock().unwrap()[1] < comparator.clone().get_listing_bounds().buffer.lock().unwrap()[1]
    }
    fn vertical_listing_bounds_greater_than
    (
        self:std::sync::Arc<Self>,
        comparator:&std::sync::Arc<crate::utilities::listing_bounds::ListingBounds>,
    ) -> bool
    {
        self.clone().clone().get_listing_bounds().buffer.lock().unwrap()[2] < comparator.clone().get_listing_bounds().buffer.lock().unwrap()[2] &&
        self.clone().clone().get_listing_bounds().buffer.lock().unwrap()[3] < comparator.clone().get_listing_bounds().buffer.lock().unwrap()[3]
    }
    fn listing_bounds_less_than_equal_to
    (
        self:std::sync::Arc<Self>,
        comparator:&std::sync::Arc<crate::utilities::listing_bounds::ListingBounds>,
    ) -> bool
    {
        self.clone().clone().get_listing_bounds().buffer.lock().unwrap()[0] <= comparator.clone().get_listing_bounds().buffer.lock().unwrap()[0] &&
        self.clone().clone().get_listing_bounds().buffer.lock().unwrap()[1] <= comparator.clone().get_listing_bounds().buffer.lock().unwrap()[1] &&
        self.clone().clone().get_listing_bounds().buffer.lock().unwrap()[2] <= comparator.clone().get_listing_bounds().buffer.lock().unwrap()[2] &&
        self.clone().clone().get_listing_bounds().buffer.lock().unwrap()[3] <= comparator.clone().get_listing_bounds().buffer.lock().unwrap()[3]
    }
    fn horizontal_listing_bounds_less_than_equal_to
    (
        self:std::sync::Arc<Self>,
        comparator:&std::sync::Arc<crate::utilities::listing_bounds::ListingBounds>,
    ) -> bool
    {
        self.clone().clone().get_listing_bounds().buffer.lock().unwrap()[0] <= comparator.clone().get_listing_bounds().buffer.lock().unwrap()[0] &&
        self.clone().clone().get_listing_bounds().buffer.lock().unwrap()[1] <= comparator.clone().get_listing_bounds().buffer.lock().unwrap()[1]
    }
    fn vertical_listing_bounds_less_than_equal_to
    (
        self:std::sync::Arc<Self>,
        comparator:&std::sync::Arc<crate::utilities::listing_bounds::ListingBounds>,
    ) -> bool
    {
        self.clone().clone().get_listing_bounds().buffer.lock().unwrap()[2] <= comparator.clone().get_listing_bounds().buffer.lock().unwrap()[2] &&
        self.clone().clone().get_listing_bounds().buffer.lock().unwrap()[3] <= comparator.clone().get_listing_bounds().buffer.lock().unwrap()[3]
    }
    fn listing_bounds_greater_than_equal_to
    (
        self:std::sync::Arc<Self>,
        comparator:&std::sync::Arc<crate::utilities::listing_bounds::ListingBounds>,
    ) -> bool
    {
        self.clone().clone().get_listing_bounds().buffer.lock().unwrap()[0] >= comparator.clone().get_listing_bounds().buffer.lock().unwrap()[0] &&
        self.clone().clone().get_listing_bounds().buffer.lock().unwrap()[1] >= comparator.clone().get_listing_bounds().buffer.lock().unwrap()[1] &&
        self.clone().clone().get_listing_bounds().buffer.lock().unwrap()[2] >= comparator.clone().get_listing_bounds().buffer.lock().unwrap()[2] &&
        self.clone().clone().get_listing_bounds().buffer.lock().unwrap()[3] >= comparator.clone().get_listing_bounds().buffer.lock().unwrap()[3]
    }
    fn horizontal_listing_bounds_greater_than_equal_to
    (
        self:std::sync::Arc<Self>,
        comparator:&std::sync::Arc<crate::utilities::listing_bounds::ListingBounds>,
    ) -> bool
    {
        self.clone().clone().get_listing_bounds().buffer.lock().unwrap()[0] >= comparator.clone().get_listing_bounds().buffer.lock().unwrap()[0] &&
        self.clone().clone().get_listing_bounds().buffer.lock().unwrap()[1] >= comparator.clone().get_listing_bounds().buffer.lock().unwrap()[1]
    }
    fn vertical_listing_bounds_greater_than_equal_to
    (
        self:std::sync::Arc<Self>,
        comparator:&std::sync::Arc<crate::utilities::listing_bounds::ListingBounds>,
    ) -> bool
    {
        self.clone().clone().get_listing_bounds().buffer.lock().unwrap()[2] >= comparator.clone().get_listing_bounds().buffer.lock().unwrap()[2] &&
        self.clone().clone().get_listing_bounds().buffer.lock().unwrap()[3] >= comparator.clone().get_listing_bounds().buffer.lock().unwrap()[3]
    }
}
impl ListingBoundsOperations for crate::utilities::listing_bounds::ListingBounds
{
    fn get_listing_bounds
    (
        self:std::sync::Arc<Self>
    ) -> std::sync::Arc<crate::utilities::listing_bounds::ListingBounds> 
    {
        self.clone()  
    }
    fn any_list_bounds
    (
        self:std::sync::Arc<Self>
    ) -> std::sync::Arc<dyn std::any::Any + Send + Sync + 'static> 
    {
        self.clone()    
    }
}
impl Clone for crate::utilities::listing_bounds::ListingBounds
{
    fn clone
    (
        &self
    ) -> Self 
    {
        crate::utilities::listing_bounds::ListingBounds
        {
            buffer:
            std::sync::Mutex::new 
            (
                vec!
                [
                        self.buffer.lock().unwrap()[0].clone(),
                        self.buffer.lock().unwrap()[1].clone(),
                        self.buffer.lock().unwrap()[2].clone(),
                        self.buffer.lock().unwrap()[3].clone(),
                ]
            ),
        }
    }
}