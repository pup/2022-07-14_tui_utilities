use thread_manager::{global_list_operations::GlobalListOperations, material_id_operations::MaterialIdOperations};

use crate::utilities_materials_operations::standard_output_operations::StandardOutputOperations;

use super::{bounds_operations::BoundsOperations, position_operations::PositionOperations, component_operations::ComponentOperations};

pub trait MenuOperations:Send + Sync
{
    fn get_menu
    (
        self:std::sync::Arc<Self>
    ) -> std::sync::Arc<crate::utilities::menu::Menu>;
    fn any_menu
    (
        self:std::sync::Arc<Self>
    ) -> std::sync::Arc<dyn std::any::Any + Send + Sync + 'static>;
    fn clear_menu
    (
        self:std::sync::Arc<Self>
    )
    {

    }
    fn menu_is_some
    (
        self:std::sync::Arc<Self>
    ) -> bool
    {
        true
    }
    fn set_menu
    (
        self:std::sync::Arc<Self>,
        menu:&std::sync::Arc<crate::utilities::menu::Menu>
    )
    {
        *self.clone().get_menu().material_id.lock().unwrap() = menu.material_id.lock().unwrap().clone();
        *self.clone().get_menu().position.lock().unwrap() = menu.position.lock().unwrap().clone();
        *self.clone().get_menu().bounds.lock().unwrap() = menu.bounds.lock().unwrap().clone();
        *self.clone().get_menu().components.lock().unwrap() = menu.components.lock().unwrap().clone();
        *self.clone().get_menu().activity.lock().unwrap() = menu.activity.lock().unwrap().clone();
        *self.clone().get_menu().active_component.lock().unwrap() = menu.active_component.lock().unwrap().clone();
    }
    fn insert_component
    (
        self:std::sync::Arc<Self>,
        index:usize,
        component:std::sync::Arc<crate::utilities::component::Component>
    )
    {
        if self.clone().get_menu().components.lock().unwrap().as_ref().unwrap().len() < index 
        {
            self.clone().get_menu().components.lock().unwrap().as_mut().unwrap().push(component.clone());
            component.clone().set_component_index(Some(self.clone().get_components_len() - 1));
            return
        }
        self.clone().get_menu().components.lock().unwrap().as_mut().unwrap().insert(index, component.clone());
        component.clone().set_component_index(Some(self.clone().get_components_len() - 1));
    }
    fn remove_component
    (
        self:std::sync::Arc<Self>,
        index:usize
    )
    {
        if  self.clone().get_menu().components.lock().unwrap().is_none() && 
            self.clone().get_menu().components.lock().unwrap().as_ref().unwrap().len() < index 
        {return}
        self.clone().get_menu().components.lock().unwrap().as_mut().unwrap().remove(index);
    }
    fn push_component
    (
        self:std::sync::Arc<Self>,
        component:std::sync::Arc<crate::utilities::component::Component>
    ) -> std::sync::Arc<crate::utilities::menu::Menu>
    {
        self.clone().get_menu().components.lock().unwrap().as_mut().unwrap().push(component.clone());
        component.clone().set_component_index(Some(self.clone().get_components_len() - 1));
        self.clone().get_menu()
    }
    fn get_components
    (
        self:std::sync::Arc<Self>
    ) -> Option<Vec<std::sync::Arc<crate::utilities::component::Component>>>
    {
        self.clone().get_menu().components.lock().unwrap().clone()
    }
    fn components_is_some
    (
        self:std::sync::Arc<Self>,
    ) -> bool 
    {
        self.clone().get_menu().components.lock().unwrap().is_some()
    }
    fn get_component_from_menu
    (
        self:std::sync::Arc<Self>,
        index:usize
    ) -> Option<std::sync::Arc<crate::utilities::component::Component>>
    {
        if self.clone().get_menu().components_is_some() {return None;}
        if index >= self.clone().get_components_len() {return None;}
        Some(self.clone().get_menu().components.lock().unwrap().as_ref().unwrap()[index].clone())
    }
    fn get_components_len
    (
        self:std::sync::Arc<Self>
    ) -> usize
    {
        self.clone().get_menu().components.lock().unwrap().as_ref().unwrap().len()
    }
    fn set_active_component
    (
        self:std::sync::Arc<Self>,
        global_list:&std::sync::Arc<thread_manager::global_list::GlobalList>,
        index:usize,
        activity:bool,
    )
    {
        let component = global_list.clone().get_material(index);
        if component.is_none() {return}
        let component_activity = component.as_ref().unwrap().clone().any_materials().downcast::<std::sync::Arc<dyn crate::utilities_operations::activity_operations::ActivityOperations>>();
        if component_activity.is_err() {return}
        component_activity.unwrap().as_ref().clone().set_active(activity);
    }
    fn get_active_component
    (
        self:std::sync::Arc<Self>
    ) -> Option<usize>
    {
        self.clone().get_menu().active_component.lock().unwrap().clone()
    }
    fn clear_background
    (
        self:std::sync::Arc<Self>,
        cursor:&std::sync::Arc<crate::utilities_materials::cursor::Cursor>,
        standard_output:&std::sync::Arc<crate::utilities_materials::standard_output::StandardOutput>,
    )
    {
        for _x in 0..self.clone().get_menu().get_horizontal_bounds_size()
        {
            for _y in 0..self.clone().get_menu().get_vertical_bounds_size()
            {
                cursor.clone().move_position_down(1);
                standard_output.clone().print_words(" ".to_string(), cursor.clone().get_position());
            }
            cursor.clone().move_position_left(1);
        }
    }
    fn collect_menu_materials
    (
        self:std::sync::Arc<Self>,
        global_list:&std::sync::Arc<thread_manager::global_list::GlobalList>,
    ) -> std::sync::Arc<thread_manager::data_space::DataSpace>
    {
        let mut memp:Vec<usize> = 
        vec!
        [
            self.clone().get_menu().clone().get_material_id_variable().unwrap()
        ];
        for index in 0..self.clone().get_components_len()
        {
            memp.push(self.clone().get_component_from_menu(index).as_ref().unwrap().clone().get_material_id_variable().unwrap());
        }
        global_list.clone().collect_materials(memp)
    }
    /// The first index of the array will be the menu 
    /// vector position. After that, the vector will 
    /// contain the vector positions of the components. 
    /// The components will be in the order that the 
    /// components were originally in.
    fn collect_menu_component_ids
    (
        self:std::sync::Arc<Self>,
    ) -> Vec<usize>
    {
        let mut temp:Vec<usize> = 
        vec!
        [
            self.clone().get_menu().clone().get_material_id_variable().unwrap()
        ];
        for index in 0..self.clone().get_components_len()
        {
            temp.push(self.clone().get_component_from_menu(index).as_ref().unwrap().clone().get_material_id_variable().unwrap());
        }
        temp
    }
}
impl MenuOperations for crate::utilities::menu::Menu
{
    fn get_menu
    (
        self:std::sync::Arc<Self>
    ) -> std::sync::Arc<crate::utilities::menu::Menu> 
    {
        self.clone()    
    }
    fn any_menu
    (
        self:std::sync::Arc<Self>
    ) -> std::sync::Arc<dyn std::any::Any + Send + Sync + 'static> 
    {
        self.clone()    
    }
}
impl crate::utilities_operations::position_operations::PositionOperations for crate::utilities::menu::Menu 
{
    fn get_position
    (
        self:std::sync::Arc<Self>
    ) -> std::sync::Arc<crate::utilities::position::Position> 
    {
        self.clone().position.lock().unwrap().as_ref().unwrap().clone()
    }
    fn any_position
    (
        self:std::sync::Arc<Self>
    ) -> std::sync::Arc<dyn std::any::Any + Send + Sync + 'static> 
    {
        self.clone()    
    }
    fn position_is_some
    (
        self:std::sync::Arc<Self>
    ) -> bool 
    {
        self.position.lock().unwrap().is_some()    
    }
    fn clear_position
    (
        self:std::sync::Arc<Self>
    ) 
    {
        *self.position.lock().unwrap() = None;    
    }
}
impl crate::utilities_operations::bounds_operations::BoundsOperations for crate::utilities::menu::Menu 
{
    fn get_bounds
    (
        self:std::sync::Arc<Self>,
    ) -> std::sync::Arc<crate::utilities::bounds::Bounds> 
    {
        self.clone().bounds.lock().unwrap().as_ref().unwrap().clone()
    }
    fn any_bounds
    (
        self:std::sync::Arc<Self>,
    ) -> std::sync::Arc<dyn std::any::Any + Send + Sync + 'static> 
    {
        self.clone()    
    }
    fn bounds_is_some
    (
        self:std::sync::Arc<Self>
    ) -> bool 
    {
        self.bounds.lock().unwrap().is_some()    
    }
    fn clear_bounds
    (
        self:std::sync::Arc<Self>
    ) 
    {
        *self.bounds.lock().unwrap() = None    
    }
}
impl crate::utilities_operations::activity_operations::ActivityOperations for crate::utilities::menu::Menu
{
    fn get_activity
    (
        self:std::sync::Arc<Self>,
    ) -> std::sync::Arc<crate::utilities::activity::Activity> 
    {
        self.clone().activity.lock().unwrap().as_ref().unwrap().clone()
    }
    fn any_activity
    (
        self:std::sync::Arc<Self>,
    ) -> std::sync::Arc<dyn std::any::Any + Send + Sync + 'static> 
    {
        self.clone()    
    }

}
impl thread_manager::material_id_operations::MaterialIdOperations for crate::utilities::menu::Menu
{
    fn get_material_id
    (
        self:std::sync::Arc<Self>,
    ) -> std::sync::Arc<thread_manager::material_id::MaterialId> 
    {
        self.clone().material_id.lock().unwrap().as_ref().unwrap().clone()    
    }
    fn any_material_id
    (
        self:std::sync::Arc<Self>,
    ) -> std::sync::Arc<dyn std::any::Any + Send + Sync + 'static> 
    {
        self.clone()    
    }
}
impl Clone for crate::utilities::menu::Menu
{
    fn clone
    (
        &self
    ) -> Self 
    {
        crate::utilities::menu::Menu
        {  
            material_id:
            std::sync::Mutex::new 
            (
                {
                    if self.material_id.lock().unwrap().is_some()
                    {
                        Some
                        (
                            std::sync::Arc::new((**self.material_id.lock().unwrap().as_ref().unwrap()).clone())
                        )
                    }
                    else 
                    {
                        None    
                    }
                }
            ),
            position:
            std::sync::Mutex::new 
            (
                {
                    if self.position.lock().unwrap().is_some()
                    {
                        Some
                        (
                            std::sync::Arc::new((**self.position.lock().unwrap().as_ref().unwrap()).clone())
                        )
                    }
                    else
                    {
                        None
                    }
                }
            ),
            bounds:
            std::sync::Mutex::new 
            (
                {
                    if self.bounds.lock().unwrap().is_some()
                    {
                        Some
                        (
                            std::sync::Arc::new((**self.bounds.lock().unwrap().as_ref().unwrap()).clone())
                        )
                    }
                    else
                    {
                        None
                    }
                }
            ),
            components:
            std::sync::Mutex::new 
            (
                {
                    if self.components.lock().unwrap().is_some()
                    {
                        Some
                        (
                                self.components.lock().unwrap().as_ref().unwrap()
                                .iter()
                                .map
                                (
                                    |component|
                                    {
                                        std::sync::Arc::new((**component).clone())
                                    }
                                )
                                .collect::<Vec<std::sync::Arc<crate::utilities::component::Component>>>()
                        )
                    }
                    else
                    {
                        None
                    }
                }
            ),
            activity:
            std::sync::Mutex::new 
            (
                {
                    if self.activity.lock().unwrap().is_some()
                    {
                        Some
                        (
                            std::sync::Arc::new((**self.activity.lock().unwrap().as_ref().unwrap()).clone())
                        )
                    }
                    else
                    {
                        None
                    }
                }
            ),
            active_component:
            std::sync::Mutex::new 
            (
                self.active_component.lock().unwrap().clone()
            ),
        }
    }
}