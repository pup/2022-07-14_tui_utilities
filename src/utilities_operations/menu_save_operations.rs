use thread_manager::data_space_operations::DataSpaceOperations;

use super::menu_operations::MenuOperations;

pub trait MenuSaveOperations
{
    fn update
    (
        self:std::sync::Arc<Self>,
        component_save_list:&Vec<std::sync::Arc<crate::utilities::component_save::ComponentSave>>,
        cursor:&std::sync::Arc<crate::utilities_materials::cursor::Cursor>,
        global_list:&std::sync::Arc<thread_manager::global_list::GlobalList>,
    );
}
impl MenuSaveOperations for crate::utilities::menu::Menu
{
    fn update
    (
        self:std::sync::Arc<Self>,
        component_save_list:&Vec<std::sync::Arc<crate::utilities::component_save::ComponentSave>>,
        cursor:&std::sync::Arc<crate::utilities_materials::cursor::Cursor>,
        global_list:&std::sync::Arc<thread_manager::global_list::GlobalList>,
    ) 
    {
        let data_space = self.collect_menu_materials(global_list);
        for j in 0..component_save_list.len()
        {
            if let Some(data) = data_space.clone().get_data(component_save_list[j].get_component_material_id())
            {
                let temp = data.clone().any_materials().clone().downcast::<std::sync::Arc<dyn crate::utilities_operations::component_save_operations::ComponentSaveOperations>>().unwrap();
                temp.as_ref().clone().update(&component_save_list[j], Some(cursor));
            }
        }
    }
}