use super::position_operations::PositionOperations;

pub trait MouseOperations
{
    fn is_left
    (
        &self
    ) -> bool;
    fn is_right 
    (
        &self
    ) -> bool;
    fn is_middle
    (
        &self
    ) -> bool;
    fn is_wheel_up
    (
        &self
    ) -> bool;
    fn is_wheel_down
    (
        &self
    ) -> bool;
    fn get_mouse_position
    (
        &self
    ) -> Option<std::sync::Arc<crate::utilities::position::Position>>;
    fn is_released
    (
        &self,
        last_button_pressed:&std::sync::Arc<crate::utilities::position::Position>,
    ) -> bool;
    fn is_held
    (
        &self,
        last_button_pressed:&std::sync::Arc<crate::utilities::position::Position>,
    ) -> bool;
}
impl MouseOperations for termion::event::MouseEvent
{
    fn is_left
    (
        &self
    ) -> bool 
    {
        std::mem::discriminant(self) == std::mem::discriminant(&termion::event::MouseEvent::Press(termion::event::MouseButton::Left, 0, 0))
    }
    fn is_right 
    (
        &self
    ) -> bool 
    {
        std::mem::discriminant(self) == std::mem::discriminant(&termion::event::MouseEvent::Press(termion::event::MouseButton::Right, 0, 0))    
    }
    fn is_middle
    (
        &self
    ) -> bool 
    {
        std::mem::discriminant(self) == std::mem::discriminant(&termion::event::MouseEvent::Press(termion::event::MouseButton::Middle, 0, 0))    
    }
    fn is_wheel_up
    (
        &self
    ) -> bool 
    {
        std::mem::discriminant(self) == std::mem::discriminant(&termion::event::MouseEvent::Press(termion::event::MouseButton::WheelUp, 0, 0))    
    }
    fn is_wheel_down
    (
        &self
    ) -> bool 
    {
        std::mem::discriminant(self) == std::mem::discriminant(&termion::event::MouseEvent::Press(termion::event::MouseButton::WheelDown, 0, 0))        
    }
    fn get_mouse_position
    (
        &self
    ) -> Option<std::sync::Arc<crate::utilities::position::Position>>
    {
        if let termion::event::MouseEvent::Press(_, x, y) = self
        {
            Some(crate::utilities::position::Position::new_position(Some(*x as usize), Some(*y as usize)))
        }
        else
        {
            None
        }
    }
    fn is_released
    (
        &self,
        last_button_pressed:&std::sync::Arc<crate::utilities::position::Position>,
    ) -> bool 
    {
        if let termion::event::MouseEvent::Release(x, y) = self
        {
            last_button_pressed.clone().get_x_position().unwrap() == *x as usize &&
            last_button_pressed.clone().get_y_position().unwrap() == *y as usize
        }
        else  
        {
            false
        }
    }
    fn is_held
    (
        &self,
        last_button_pressed:&std::sync::Arc<crate::utilities::position::Position>,
    ) -> bool 
    {
        if let termion::event::MouseEvent::Hold(x, y) = self
        {
            last_button_pressed.clone().get_x_position().unwrap() == *x as usize &&
            last_button_pressed.clone().get_y_position().unwrap() == *y as usize
        }
        else  
        {
            false
        }   
    }
}
