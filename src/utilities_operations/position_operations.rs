pub trait PositionOperations
{
    fn get_position
    (
        self:std::sync::Arc<Self>
    ) -> std::sync::Arc<crate::utilities::position::Position>;
    fn any_position
    (
        self:std::sync::Arc<Self>
    ) -> std::sync::Arc<dyn std::any::Any + Send + Sync + 'static>;
    fn clear_position
    (
        self:std::sync::Arc<Self>
    )
    {

    }
    fn position_is_some
    (
        self:std::sync::Arc<Self>
    ) -> bool
    {
        true
    }
    fn set_position
    (
        self:std::sync::Arc<Self>, 
        position:&std::sync::Arc<crate::utilities::position::Position>
    )
    {
        *self.clone().get_position().x.lock().unwrap() = position.x.lock().unwrap().clone();
        *self.clone().get_position().y.lock().unwrap() = position.y.lock().unwrap().clone();
    }
    fn get_x_position
    (
        self:std::sync::Arc<Self>,
    ) -> Option<usize>
    {
        self.clone().get_position().x.lock().unwrap().clone()
    }
    fn get_y_position
    (
        self:std::sync::Arc<Self>,
    ) -> Option<usize>
    {
        self.clone().get_position().y.lock().unwrap().clone()
    }
    fn set_x_position
    (
        self:std::sync::Arc<Self>, 
        value:Option<usize>
    )
    {
        *self.clone().get_position().x.lock().unwrap() = value;
    }
    fn set_y_position
    (
        self:std::sync::Arc<Self>, 
        value:Option<usize>
    )
    {
        *self.clone().get_position().y.lock().unwrap() = value;
    }
    fn x_position_is_some
    (
        self:std::sync::Arc<Self>,
    ) -> bool
    {
        self.clone().get_position().x.lock().unwrap().is_some()
    }
    fn y_position_is_some
    (
        self:std::sync::Arc<Self>,
    ) -> bool
    {
        self.clone().get_position().y.lock().unwrap().is_some()
    }
    fn has_x_position_and_y_position
    (
        self:std::sync::Arc<Self>,
    ) -> bool
    {
        self.clone().get_position().x_position_is_some() && self.clone().get_position().y_position_is_some()
    }
    fn increment_x_position
    (
        self:std::sync::Arc<Self>, 
        value:usize
    )
    {
        self.clone().set_x_position(Some(self.clone().get_x_position().unwrap() + value));
    }
    fn decrement_x_position
    (
        self:std::sync::Arc<Self>, 
        value:usize
    )
    {
        if self.clone().get_x_position().unwrap() != 0 && self.clone().get_x_position().unwrap().checked_sub(value).is_some()
        {
            self.clone().set_x_position(Some(self.clone().get_x_position().unwrap() - value));
        }
        else
        {
            self.clone().set_x_position(Some(0));
        }
    }
    fn increment_y_position
    (
        self:std::sync::Arc<Self>, 
        value:usize
    )
    {
        self.clone().set_y_position(Some(self.clone().get_y_position().unwrap() + value));
    }
    fn decrement_y_position
    (
        self:std::sync::Arc<Self>, 
        value:usize
    )
    {
        if self.clone().get_y_position().unwrap() != 0 && self.clone().get_y_position().unwrap().checked_sub(value).is_some()
        {
            self.clone().set_y_position(Some(self.clone().get_y_position().unwrap() - value));
        }
        else
        {
            self.clone().set_y_position(Some(0));
        }
    }
    fn move_position_left
    (
        self:std::sync::Arc<Self>, 
        value:usize
    )
    {
        self.clone().decrement_x_position(value);
    }
    fn move_position_right
    (
        self:std::sync::Arc<Self>, 
        value:usize
    )
    {
        self.clone().increment_x_position(value);
    }
    fn move_position_up
    (
        self:std::sync::Arc<Self>, 
        value:usize
    )
    {
        self.clone().decrement_y_position(value);
    }
    fn move_position_down
    (
        self:std::sync::Arc<Self>, 
        value:usize
    )
    {
        self.clone().increment_y_position(value);
    }
    fn positions_have_x_position_and_y_position
    (
        self:std::sync::Arc<Self>,
        list:Vec<&std::sync::Arc<crate::utilities::position::Position>>,
    ) -> bool
    {
        if !self.clone().has_x_position_and_y_position() {return false;}
        for i in 0..list.len()
        {
            if !list[i].clone().has_x_position_and_y_position() {return false;}
        }
        true
    }
}
impl PositionOperations for crate::utilities::position::Position
{
    fn get_position
    (
        self:std::sync::Arc<Self>
    ) -> std::sync::Arc<crate::utilities::position::Position> 
    {
        self.clone()    
    }
    fn any_position
    (
        self:std::sync::Arc<Self>
    ) -> std::sync::Arc<dyn std::any::Any + Send + Sync + 'static> 
    {
        self.clone()    
    }
}
impl Clone for crate::utilities::position::Position
{
    fn clone
    (
        &self
    ) -> crate::utilities::position::Position
    {
        crate::utilities::position::Position
        {
            x:
            std::sync::Mutex::new
            (
                self.x.lock().unwrap().clone()
            ),
            y:
            std::sync::Mutex::new
            (
                self.y.lock().unwrap().clone()
            ),
        }
    }
}