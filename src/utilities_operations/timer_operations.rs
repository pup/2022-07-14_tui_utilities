pub trait TimerOperations
{
    fn get_timer
    (
        self:std::sync::Arc<Self>
    ) -> std::sync::Arc<crate::utilities::timer::Timer>;
    fn any_timer
    (
        self:std::sync::Arc<Self>
    ) -> std::sync::Arc<dyn std::any::Any + Send + Sync + 'static>;
    fn clear_timer
    (
        self:std::sync::Arc<Self>
    )
    {

    }
    fn timer_is_some
    (
        self:std::sync::Arc<Self>
    ) -> bool
    {
        true
    }
    fn set_timer
    (
        self:std::sync::Arc<Self>,
        timer:&std::sync::Arc<crate::utilities::timer::Timer>
    )
    {
        *self.clone().get_timer().system_time.lock().unwrap() = timer.system_time.lock().unwrap().clone();
        *self.clone().get_timer().duration.lock().unwrap() = timer.duration.lock().unwrap().clone();
        *self.clone().get_timer().triggered.lock().unwrap() = timer.triggered.lock().unwrap().clone();
        *self.clone().get_timer().done.lock().unwrap() = timer.done.lock().unwrap().clone();
    }
    fn start_timer
    (
        self:std::sync::Arc<Self>
    )
    {
        if self.clone().get_timer().get_triggered() == false
        {
            *self.clone().get_timer().system_time.lock().unwrap() = Some(std::time::SystemTime::now());
            self.clone().get_timer().set_triggered(true);
            self.clone().get_timer().set_done(false);
        }
    }
    fn stop_timer
    (
        self:std::sync::Arc<Self>
    )
    {
        if self.clone().get_timer().get_triggered() == true
        {
            *self.clone().get_timer().system_time.lock().unwrap() = None;
            self.clone().get_timer().set_triggered(false);
            self.clone().get_timer().set_done(true);
        }
    }
    fn set_timer_duration
    (
        self:std::sync::Arc<Self>, 
        value:Option<std::time::Duration>
    )
    {
        *self.clone().get_timer().duration.lock().unwrap() = value;
    }
    fn get_timer_duration
    (
        self:std::sync::Arc<Self>,
    ) -> Option<std::time::Duration>
    {
        self.clone().get_timer().duration.lock().unwrap().clone()
    }
    fn timer_is_done
    (
        self:std::sync::Arc<Self>
    ) -> bool
    {
        if  self.clone().get_timer().system_time_equals_duration() &&
            self.clone().get_timer().get_triggered()
        {
            self.clone().stop_timer();
            self.clone().get_timer().get_done()
        }
        else 
        {
            self.clone().get_timer().get_done()    
        }
    }
}
impl TimerOperations for crate::utilities::timer::Timer
{
    fn get_timer
    (
        self:std::sync::Arc<Self>
    ) -> std::sync::Arc<crate::utilities::timer::Timer> 
    {
        self.clone()    
    }
    fn any_timer
    (
        self:std::sync::Arc<Self>
    ) -> std::sync::Arc<dyn std::any::Any + Send + Sync + 'static> 
    {
        self.clone()    
    }
}
impl Clone for crate::utilities::timer::Timer
{
    fn clone
    (
        &self
    ) -> crate::utilities::timer::Timer 
    {
        crate::utilities::timer::Timer
        {
            system_time:
            std::sync::Mutex::new 
            (
                None
            ),
            duration:
            std::sync::Mutex::new 
            (
                self.duration.lock().unwrap().clone()
            ),
            triggered:
            std::sync::Mutex::new 
            (
                false
            ),
            done:
            std::sync::Mutex::new 
            (
                false
            ),
        }    
    }
}