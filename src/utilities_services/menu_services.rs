pub trait MenuServices
{
    fn on_tab_menu
	(
	    materials:&Vec<std::sync::Arc<dyn thread_manager::materials_operations::MaterialsOperations>>
	);
}